import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/pages/splashPage.dart';
import 'package:upSkill/states/authenticationState.dart';
import 'package:upSkill/states/coursesState.dart';
import 'package:upSkill/states/postsState.dart';
import 'package:upSkill/states/profileState.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:upSkill/states/reviewsState.dart';
import 'package:upSkill/states/searchState.dart';
import 'package:upSkill/states/subjectsState.dart';

Future<void> main() async {
  await DotEnv.load(fileName: ".env");
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => AuthenticationState()),
        ChangeNotifierProvider(create: (context) => ProfileState()),
        ChangeNotifierProvider(create: (context) => SubjectsState()),
        ChangeNotifierProvider(create: (context) => CoursesState()),
        ChangeNotifierProvider(create: (context) => SearchState()),
        ChangeNotifierProvider(create: (context) => ReviewsState()),
        ChangeNotifierProvider(create: (context) => PostsState()),
      ],
      child: MyApp(),
    ),
  );
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    configEasyLoading();
  }

  void configEasyLoading() {
    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 2000)
      ..indicatorType = EasyLoadingIndicatorType.pulse
      ..loadingStyle = EasyLoadingStyle.custom
      ..indicatorSize = 30.0
      ..radius = 10.0
      ..progressColor = Color(0xFFF400CF)
      ..backgroundColor = Colors.white
      ..indicatorColor = Color(0xFFF400CF)
      ..textColor = Color(0xFFF400CF)
      ..maskType = EasyLoadingMaskType.black
      ..maskColor = Colors.blue.withOpacity(0.5)
      ..userInteractions = false
      ..dismissOnTap = false;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: EasyLoading.init(
        builder: (context, child) => MediaQuery(data: MediaQuery.of(context), child: child),
      ),
      home: SplashPage(),
      debugShowCheckedModeBanner: false,
      title: "upSkill",
    );
  }
}

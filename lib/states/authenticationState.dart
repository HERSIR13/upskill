import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:upSkill/models/userModel.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';
import 'package:upSkill/utils/helpers/utilsHelper.dart';

class AuthenticationState extends ChangeNotifier {
  String authToken;
  bool _isLoading = false;

  bool get isLoading => _isLoading;

  Future<Map> register({String email, String password, String name}) async {
    _isLoading = true;
    notifyListeners();
    if (!await UtilsHelper.hasInternet()) {
      _isLoading = false;

      notifyListeners();
    } else {
      try {
        String _authToken = await SharedPreferencesHelper.getAuthToken();

        User user = User.register(email: email, password: password, name: name);
        var response = await user.registerUser(authToken: _authToken);
        if (response.statusCode == 200) {
          final Map<String, dynamic> parsed = json.decode(response.body);
          await SharedPreferencesHelper.saveEmailPassword(email: email, password: password);
          await SharedPreferencesHelper.setGuestStatus(isGuest: false);
          _isLoading = false;
          notifyListeners();
          return {
            'error': false,
            'response': parsed["user"],
          };
        } else {
          final Map<String, dynamic> parsed = json.decode(response.body);
          _isLoading = false;
          notifyListeners();
          return {'error': true, 'errors': parsed['errors']};
        }
      } catch (e) {
        _isLoading = false;
        notifyListeners();
        throw (e);
      }
    }
    return {'error': true, 'errors': 'weow'};
  }

  Future<Map> login({String email, String password}) async {
    _isLoading = true;
    notifyListeners();
    if (!await UtilsHelper.hasInternet()) {
      _isLoading = false;
      notifyListeners();
    } else {
      try {
        User user = User.login(email: email, password: password);
        var response = await user.loginUser();
        if (response.statusCode == 200) {
          final Map<String, dynamic> parsed = json.decode(response.body);
          authToken = parsed['token'];
          await SharedPreferencesHelper.saveAuthToken(token: authToken);
          await SharedPreferencesHelper.saveEmailPassword(email: email, password: password);
          await SharedPreferencesHelper.setGuestStatus(isGuest: false);
          EasyLoading.dismiss();
          notifyListeners();
          return {
            'error': false,
            'response': parsed["user"],
          };
        } else {
          final Map<String, dynamic> parsed = json.decode(response.body);
          EasyLoading.dismiss();
          notifyListeners();
          return {'error': true, 'errors': parsed['errors']};
        }
      } catch (e) {
        EasyLoading.dismiss();
        notifyListeners();
        throw (e);
      }
    }
    return null;
  }

  Future<Map> guestLogin({String email, String password}) async {
    _isLoading = true;
    notifyListeners();
    if (!await UtilsHelper.hasInternet()) {
      _isLoading = false;
      notifyListeners();
    } else {
      try {
        User user = User.guestLogin();
        var response = await user.guestLoginUser();
        if (response.statusCode == 200) {
          final Map<String, dynamic> parsed = json.decode(response.body);
          authToken = parsed['token'];
          await SharedPreferencesHelper.saveAuthToken(token: authToken);
          await SharedPreferencesHelper.setGuestStatus(isGuest: true);
          EasyLoading.dismiss();
          notifyListeners();
          return {
            'error': false,
            'response': parsed["user"],
          };
        } else {
          final Map<String, dynamic> parsed = json.decode(response.body);
          EasyLoading.dismiss();
          notifyListeners();
          return {'error': true, 'errors': parsed['errors']};
        }
      } catch (e) {
        EasyLoading.dismiss();
        notifyListeners();
        throw (e);
      }
    }
    return null;
  }

  Future<Map> logout(BuildContext context) async {
    notifyListeners();
    if (!await UtilsHelper.hasInternet()) {
      notifyListeners();
    } else {
      try {
        String _authToken = await SharedPreferencesHelper.getAuthToken();
        var response = await User.logoutUser(authToken: _authToken);
        if (response.statusCode == 200) {
          await SharedPreferencesHelper.saveAuthToken(token: '');
          authToken = null;
          SharedPreferencesHelper.resetLatestActivity();
          SharedPreferencesHelper.saveEmailPassword(email: '', password: '');
          notifyListeners();
          return {'error': false};
        } else {
          final Map<String, dynamic> parsed = json.decode(response.body);
          notifyListeners();
          return {'error': true, 'errors': parsed['errors']};
        }
      } catch (e) {
        throw Exception(e);
      }
    }
    return null;
  }

}

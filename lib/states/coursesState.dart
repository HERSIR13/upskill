import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:upSkill/models/courseModel.dart';
import 'package:upSkill/models/instructorModel.dart';
import 'package:upSkill/models/lectureModel.dart';
import 'package:upSkill/models/reviewModel.dart';
import 'package:upSkill/utils/constants.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';
import 'package:http/http.dart' as http;

class CoursesState extends ChangeNotifier {
  Future<Map<String, dynamic>> getCourseData(int courseId) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();
    Uri uri = Links.getCourseDataUri(courseId, 1, 1);
    final res = await http.get(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );
    if (res.statusCode == 200) {
      final data = jsonDecode(res.body);
      final coursesData = data["course"];
      Course course = setCourse(coursesData, true, true, true);
      List<Course> recommendedCourses = [];
      coursesData["recommended_courses"].forEach((courseFromData) {
        Course recommendedCourse = setCourse(courseFromData, false, true, false);
        recommendedCourses.add(recommendedCourse);
      });
      return {"course": course, "recommendedCourses": recommendedCourses};
    } else {
      return null;
    }
  }

  Course setCourse(Map<String, dynamic> courseData, bool withLectures, bool withInstructor, bool withReviews) {
    Course course = new Course();
    course.id = courseData["id"];
    course.instructorId = courseData["instructor_id"];
    course.subjectId = courseData["subject_id"];
    course.name = courseData["name"];
    course.image = courseData["image"];
    course.introductoryVideo = courseData["introductory_video"];
    course.description = courseData["description"];
    course.rating = courseData["rating"];
    course.price = courseData["price"];
    course.objectives = courseData["objectives"];
    course.length = courseData["length"];
    course.level = courseData["level"];
    course.nbStudentsEnrolled = courseData["students_enrolled"];
    course.isBestSeller = courseData["best_seller"];
    course.isNew = courseData["new"];
    course.positivity = courseData["positivity"].toInt();

    if (withInstructor) {
      final instructorData = courseData["instructor"];
      Instructor courseInstructor = new Instructor();
      courseInstructor.id = instructorData["id"];
      courseInstructor.name = instructorData["name"];
      courseInstructor.email = instructorData["email"];
      courseInstructor.avatar = instructorData["avatar"];
      courseInstructor.biography = instructorData["biography"];
      courseInstructor.introductoryVideo = instructorData["introductory_video"];
      courseInstructor.interests = instructorData["interests"];
      courseInstructor.education = instructorData["education"];
      courseInstructor.job = instructorData["job"];
      courseInstructor.specialization = instructorData["specialization"];
      courseInstructor.achievements = instructorData["achievements"];
      course.instructor = courseInstructor;
    }
    if (withLectures) {
      List<Lecture> lectures = [];
      courseData["lectures"].forEach((lectureData) {
        Lecture courseLecture = new Lecture();
        courseLecture.id = lectureData["id"];
        courseLecture.name = lectureData["name"];
        courseLecture.video = lectureData["video"];
        courseLecture.description = lectureData["description"];
        courseLecture.duration = lectureData["duration"];
        courseLecture.courseId = lectureData["course_id"];
        lectures.add(courseLecture);
      });
      course.lectures = lectures;
    }
    if (withReviews) {
      List<Review> reviews = [];
      courseData["course_reviews"].forEach((reviewData) {
        Review review = new Review();
        review.id = reviewData["id"];
        review.courseId = reviewData["course_id"];
        review.studentId = reviewData["student_id"];
        review.description = reviewData["description"];
        review.title = reviewData["title"];
        review.studentName = reviewData["student"]["name"];
        review.rating = reviewData["rating"];
        reviews.add(review);
      });
      course.reviews = reviews;
    }
    return course;
  }

  Future<Map<String, dynamic>> subscribeCourse(int courseId) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();
    Uri uri = Links.subscribeCourseUri(courseId);
    final res = await http.post(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );
    if (res.statusCode == 200) {
      return {"error": false};
    } else {
      return {"error": true};
    }
  }
}

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:upSkill/utils/constants.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';
import 'package:http/http.dart' as http;

class ReviewsState extends ChangeNotifier {
  Future<Map<String, dynamic>> postReview(int courseId, String title, String description, int rating) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();
    Uri uri = Links.postReviewUri(courseId, title, description, rating);
    final res = await http.post(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );
    final decodedData = jsonDecode(res.body);
    if (res.statusCode == 200) {
      return {"error": false, 'id': decodedData["review"]["id"]};
    } else {
      return {"error": true};
    }
  }
}

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/models/courseModel.dart';
import 'package:upSkill/models/subjectModel.dart';
import 'package:upSkill/states/coursesState.dart';
import 'package:upSkill/utils/constants.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';
import 'package:http/http.dart' as http;

class SearchState extends ChangeNotifier {
  Future<Map<String, dynamic>> getExploreSearchData(String search, context) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();

    Uri uri = Links.getExploreSearchDataUri(search, 1);
    final res = await http.get(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );
    final decodedData = jsonDecode(res.body);

    if (res.statusCode == 200) {
      List<Subject> searchSubjects = [];
      if (decodedData["subjects"].isNotEmpty) {
        final subjectsData = decodedData["subjects"];
        subjectsData.forEach((subjectData) {
          Subject searchSubject = new Subject();
          searchSubject.id = subjectData["id"];
          searchSubject.name = subjectData["name"];
          searchSubject.image = subjectData["image"];
          searchSubject.introductoryVideo = subjectData["introductory_video"];
          searchSubjects.add(searchSubject);
        });
      }
      List<Course> searchCourses = [];
      if (decodedData["courses"].isNotEmpty) {
        final coursesData = decodedData["courses"];
        coursesData.forEach((courseData) {
          Course searchCourse = Provider.of<CoursesState>(context, listen: false).setCourse(courseData, false, true, false);
          searchCourses.add(searchCourse);
        });
      }
      return {'subjects': searchSubjects, 'courses': searchCourses};
    }
    return null;
  }
}

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:upSkill/models/commentModel.dart';
import 'package:upSkill/models/postModel.dart';
import 'package:upSkill/utils/constants.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class PostsState extends ChangeNotifier {
  Future<Map<String, dynamic>> getPostsData(context) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();

    Uri uri = Links.getPostsUri(1, 1, 0);
    final res = await http.get(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );
    final decodedData = jsonDecode(res.body);
    if (res.statusCode == 200) {
      final postsData = decodedData['posts'];
      List<Post> allPosts = [];
      postsData.forEach((postData) {
        Post newPost = new Post();
        newPost.id = postData['id'];
        newPost.subjectId = postData['subject_id'];
        newPost.studentId = postData['student_id'];
        newPost.title = postData['title'];
        newPost.description = postData['description'];
        newPost.image = postData['image'];
        newPost.views = postData['views'];
        newPost.votesUp = postData['votes_up'];
        newPost.votesDown = postData['votes_down'];
        newPost.bestCommentId = postData['best_comment_id'];
        newPost.studentName = postData['student']['name'];
        newPost.subjectName = postData['subject']['name'];
        allPosts.add(newPost);
      });
      return {'error': false, 'posts': allPosts};
    } else {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text("Something went wrong, try again later"),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'okay',
                style: TextStyle(
                  color: Theme.of(context).secondaryHeaderColor,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Future<Map<String, dynamic>> getPostData(context, int postId) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();

    Uri uri = Links.getPostDataUri(postId, 1, 1, 1);
    final res = await http.get(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );
    final decodedData = jsonDecode(res.body);
    if (res.statusCode == 200) {
      final postData = decodedData['post'];
      Post post = new Post();
      post.id = postData['id'];
      post.subjectId = postData['subject_id'];
      post.studentId = postData['student_id'];
      post.title = postData['title'];
      post.description = postData['description'];
      post.image = postData['image'];
      post.views = postData['views'];
      post.votesUp = postData['votes_up'];
      post.votesDown = postData['votes_down'];
      post.bestCommentId = postData['best_comment_id'];
      post.studentName = postData['student']['name'];
      post.subjectName = postData['subject']['name'];
      post.comments = [];
      postData['comments'].forEach((commentData) {
        Comment comment = new Comment();
        comment.id = commentData['id'];
        comment.studentId = commentData['student_id'];
        comment.title = commentData['title'];
        comment.description = commentData['description'];
        comment.image = commentData['image'];
        comment.votesUp = commentData['votes_up'];
        comment.votesDown = commentData['votes_down'];
        comment.bestCommentId = commentData['best_comment'];
        comment.studentName = commentData['student']['name'];
        comment.studentImage = commentData['student']['avatar'];
        post.comments.add(comment);
      });
      return {'error': false, 'post': post};
    } else {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text("Something went wrong, try again later"),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'okay',
                style: TextStyle(
                  color: Theme.of(context).secondaryHeaderColor,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Future<Map<String, dynamic>> storePostOrComment(context, int id, String title, String description, {File image, isComment = false}) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();
    if (isComment) title = ".";

    Uri uri = isComment ? Links.storeCommentUri : Links.storePostUri;
    var request = http.MultipartRequest("POST", uri);
    request.headers['authorization'] = "Bearer $_authToken";
    request.headers['Content-Type'] = "multipart/form-data";
    if (image != null)
      request.files.add(await http.MultipartFile.fromPath('imageFile', image.path, contentType: MediaType('image', 'jpg'), filename: "test.jpg"));

    String idField = isComment ? "post_id" : "subject_id";

    request.fields.addAll({
      idField: id.toString(),
      "title": title,
      "description": description,
    });
    return request.send().then((res) async {
      // final bodyEncoded = await res.stream.bytesToString();
      // final decodedData = jsonDecode(bodyEncoded);
      // final postData = decodedData['post'];
      if (res.statusCode == 200) {
        return {
          'error': false,
        };
      } else {
        return {
          'error': true,
        };
      }
    }).catchError((e) {
      print(e);
      return null;
    });
  }

  Future<Map<String, dynamic>> votePostOrComment(context, int id, bool isComment, String vote) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();

    Uri uri = isComment ? Links.voteCommentUri(id, 0, vote) : Links.votePostUri(id, 0, 0, 0, vote);
    final res = await http.get(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );
    if (res.statusCode == 200) {
      return {'error': false};
    } else {
      return {'error': true};
    }
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/models/courseModel.dart';
import 'package:upSkill/models/subjectModel.dart';
import 'package:upSkill/states/coursesState.dart';
import 'package:upSkill/utils/constants.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class SubjectsState extends ChangeNotifier {
  List<Subject> allSubjects = [];

  Future<void> setSubjects(context) async {
    allSubjects.clear();
    String _authToken = await SharedPreferencesHelper.getAuthToken();
    Uri uri = Links.getSubjectsUri;
    final res = await http.get(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );

    if (res.statusCode == 200) {
      final decodedData = jsonDecode(res.body);
      final List<dynamic> newSubjects = decodedData["subjects"];
      newSubjects.forEach(
        (newSubject) {
          Subject subject = setSubjectData(newSubject);
          subject.courses = [];
          allSubjects.add(subject);
        },
      );
      notifyListeners();
    } else {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text("Something went wrong, try again later"),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'okay',
                style: TextStyle(
                  color: Theme.of(context).secondaryHeaderColor,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Future<Subject> getSubjectData(context, int subjectId) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();
    Uri uri = Links.getSubjectDataUri(subjectId, 1, 1);
    final res = await http.get(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );
    if (res.statusCode == 200) {
      final data = jsonDecode(res.body);
      final subjectData = data["subject"];
      Subject chosenSubject = setSubjectData(subjectData);
      chosenSubject.courses = [];
      subjectData["courses"].forEach((courseFromData) {
        Course course = Provider.of<CoursesState>(context, listen: false).setCourse(courseFromData, true, true, false);
        chosenSubject.courses.add(course);
      });
      return chosenSubject;
    } else {
      return null;
    }
  }

  Future<Map<String, dynamic>> getExploreData(context) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();

    Uri uri = Links.getExploreDataUri(0);
    final res = await http.get(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );
    if (res.statusCode == 200) {
      final data = jsonDecode(res.body);
      final exploreCoursesData = data["courses"];
      final exploreSubjectsData = data["subjects"];
      List<Subject> subjects = [];
      exploreSubjectsData.forEach((subjectData) {
        Subject newSubject = setSubjectData(subjectData);
        subjects.add(newSubject);
      });
      List<Course> recommendedCourses = [];
      exploreCoursesData.forEach((courseFromData) {
        Course course = Provider.of<CoursesState>(context, listen: false).setCourse(courseFromData, false, true, false);
        recommendedCourses.add(course);
      });
      return {"recommended": recommendedCourses, "subjects": subjects};
    } else {
      return null;
    }
  }

  Subject setSubjectData(subjectData) {
    Subject subject = new Subject();
    subject.id = subjectData["id"];
    subject.name = subjectData["name"];
    subject.image = subjectData["image"];
    subject.introductoryVideo = subjectData["introductory_video"];
    return subject;
  }
}

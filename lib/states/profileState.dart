import 'package:upSkill/models/profileModel.dart';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:upSkill/utils/constants.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/models/courseModel.dart';
import 'package:upSkill/states/coursesState.dart';

class ProfileState extends ChangeNotifier {
  Profile profile = new Profile();

  Future<void> getProfileData(context, isGuest) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();

    Uri uri = Links.getProfileDataUri(profile.id, 1, 1);
    final res = await http.get(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );
    final decodedData = jsonDecode(res.body);
    final studentData = decodedData["student"];
    setProfile(studentData, context, isUpdate: false, isGuest: false);
  }

  void setProfile(data, context, {isUpdate, isGuest}) async {
    if (isGuest == false) {
      profile.id = data["id"];
      profile.roleId = data["role_id"];
      profile.email = data["email"];
      profile.name = data["name"];
      profile.avatar = data["avatar"];
      profile.biography = data["biography"] ?? "Not Specified";
      profile.interests = data["interests"].join(",");
      profile.subscribedCourses = [];
      if (isUpdate != null) {
        final subscribedCoursesData = data["coursesSubscribed"];
        subscribedCoursesData.forEach((subscribedCourse) {
          Course course = Provider.of<CoursesState>(context, listen: false).setCourse(subscribedCourse, false, true, false);
          profile.subscribedCourses.add(course);
        });
      }
      notifyListeners();
    } else {
      profile.id = data["id"];
      profile.name = data["name"];
      profile.biography = data["biography"] ?? "Not Specified";
      profile.avatar = data["avatar"];
      profile.interests = data["interests"].join(",");
      notifyListeners();
    }
  }

  Future<void> updateProfile({String name, String biography, String interests, context}) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();
    Uri uri = Links.updateStudentUri;
    Map<String, dynamic> body = {};
    if (name != null) body.addAll({'name': name});
    if (biography != null) body.addAll({'biography': biography});
    if (interests != null) body.addAll({'interests': interests});
    final res = await http.post(
      uri,
      body: jsonEncode(body),
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $_authToken'},
    );
    final decodedData = jsonDecode(res.body);
    final studentData = decodedData["student"];
    setProfile(studentData, context, isUpdate: true);
  }
}

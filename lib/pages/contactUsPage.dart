//Karim's version
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:upSkill/config/contactUsData.dart';
import 'package:upSkill/widgets/logoAppBar.dart';

class ContactUsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final data = ContactUsData.data;
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: LogoAppBar.appBar,
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: ListView.separated(
          shrinkWrap: true,
          separatorBuilder: (context, i) => SizedBox(
            height: 15,
          ),
          itemBuilder: (context, i) => ListTile(
            tileColor: Colors.white,
            leading: Icon(
              data[i]["icon"],
              color: Colors.blue,
              size: 35,
            ),
            title: Text(
              data[i]["title"],
              style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            subtitle: Text(
              data[i]["details"],
            ),
          ),
          itemCount: data.length,
        ),
      ),
    );
  }
}

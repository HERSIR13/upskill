import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:upSkill/models/courseModel.dart';
import 'package:upSkill/pages/coursePage.dart';
import 'package:upSkill/states/profileState.dart';

class MyCoursesPage extends StatefulWidget {
  @override
  _MyCoursesPageState createState() => _MyCoursesPageState();
}

class _MyCoursesPageState extends State<MyCoursesPage> {
  List filteredCourses;
  bool isCompletedChosen;
  bool isInCompleteChosen;
  bool allChosen;
  List<Course> myCourses = [];

  @override
  void initState() {
    _setMyCourses();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new LayoutBuilder(
      builder: (context, constraints) => Container(
        color: Colors.grey[200],
        height: constraints.maxHeight,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Container(
                child: GridView.count(
                  padding: EdgeInsets.only(
                    bottom: 10,
                    top: 10,
                    right: 5,
                    left: 5,
                  ),
                  childAspectRatio: 1.5,
                  primary: false,
                  crossAxisCount: 2,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 5,
                  children: List.generate(myCourses.length, (i) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(
                          SlideRightRoute(
                            page: CoursePage(
                              courseId: myCourses[i].id,
                            ),
                          ),
                        );
                      },
                      child: Center(
                          child: Container(
                        color: Colors.white,
                        width: 180,
                        height: 200,
                        child: new LayoutBuilder(
                          builder: (context, constraints) => Stack(
                            children: [
                              Container(
                                child: ProgressiveImage(
                                  placeholder: AssetImage('assets/images/Logo.png'),
                                  // size: 1.87KB
                                  thumbnail: AssetImage('assets/images/Logo.png'),
                                  // size: 1.29MB
                                  image: NetworkImage(myCourses[i].image),
                                  width: constraints.maxWidth,
                                  height: 200,
                                ),
                              ),
                              Positioned(
                                bottom: 0,
                                left: 0,
                                child: Container(
                                  width: constraints.maxWidth,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      colors: [
                                        Color(0xFF6380E4),
                                        Color(0xFFA547DA),
                                      ],
                                    ),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          myCourses[i].name,
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w800, color: Colors.white, fontSize: 13),
                                        ),
                                        SizedBox(
                                          height: 3,
                                        ),
                                        Text(
                                          myCourses[i].instructor.name,
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w400, color: Colors.white, fontSize: 10),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )),
                    );
                  }),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _setMyCourses() {
    setState(() {
      myCourses = Provider.of<ProfileState>(context, listen: false).profile.subscribedCourses;
    });
  }
}

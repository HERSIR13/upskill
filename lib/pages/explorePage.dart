import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/models/courseModel.dart';
import 'package:upSkill/models/subjectModel.dart';
import 'package:upSkill/pages/subjectPage.dart';
import 'package:upSkill/states/searchState.dart';
import 'package:upSkill/states/subjectsState.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';
import 'package:upSkill/widgets/courseTile.dart';
import 'package:upSkill/widgets/logoAppBar.dart';
import 'package:upSkill/widgets/subjectListView.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class ExplorePage extends StatefulWidget {
  @override
  _ExplorePageState createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  bool isSearchEnabled = false;
  TextEditingController searchBarController = new TextEditingController();
  List<Subject> searchSubjects = [];
  List<Course> searchCourses = [];

  bool isLoading = true;
  List<Subject> allSubjects = [];
  List<Course> recommendedCourses = [];
  List<Course> popularCourses = [];
  bool isGuest = true;

  @override
  void initState() {
    _getGuestStatus();
    _readyExplorePage();
    super.initState();
  }

  Future<void> _getGuestStatus() async {
    isGuest = await SharedPreferencesHelper.getGuestStatus();
    if (isGuest == false) setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> backButtonPressed() async {
    if (isSearchEnabled)
      setState(() {
        isSearchEnabled = false;
      });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: isGuest ? LogoAppBar.appBar : null,
      body: !isLoading
          ? WillPopScope(
              onWillPop: () => backButtonPressed(),
              child: Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Container(
                        color: Color(0xFFF0F0F0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(30, 25, 30, 0),
                              child: Container(
                                color: Colors.white,
                                width: MediaQuery.of(context).size.width - 50,
                                height: 40,
                                child: Container(
                                  padding: EdgeInsets.only(left: 20, right: 20),
                                  width: width - 100,
                                  child: TextFormField(
                                    controller: searchBarController,
                                    onFieldSubmitted: (text) {
                                      if (searchBarController.text.isNotEmpty) {
                                        _search(text);
                                        if (!isSearchEnabled)
                                          setState(() {
                                            isSearchEnabled = true;
                                          });
                                      } else if (isSearchEnabled)
                                        setState(() {
                                          isSearchEnabled = false;
                                        });
                                    },
                                    decoration: InputDecoration(
                                      hintText: "What do you want to learn?",
                                      hintStyle: GoogleFonts.roboto(fontSize: 12, color: Color(0xFF888888)),
                                      border: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      contentPadding: EdgeInsets.only(top: 7),
                                      prefixIcon: Icon(
                                        Icons.search,
                                        color: Color(0xFFBCBCBC),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            if (isSearchEnabled)
                              Column(
                                children: [
                                  SubjectListView(
                                    subjects: searchSubjects,
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(vertical: 20),
                                    height: 325,
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, i) {
                                        return CourseTile(
                                          width: width,
                                          isLast: i == searchCourses.length - 1 ? true : false,
                                          tileIndex: i,
                                          course: searchCourses[i],
                                        );
                                      },
                                      itemCount: searchCourses.length,
                                    ),
                                  ),
                                ],
                              ),
                            if (!isSearchEnabled)
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.only(left: 25, right: 25, top: 30),
                                    child: Text(
                                      "Recommended for you!",
                                      style: GoogleFonts.roboto(
                                          fontSize: (width / 20).ceil().toDouble(), color: Colors.black87, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    height: 325,
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, i) {
                                        return CourseTile(
                                          width: width,
                                          isLast: i == recommendedCourses.length - 1 ? true : false,
                                          tileIndex: i,
                                          course: recommendedCourses[i],
                                        );
                                      },
                                      itemCount: recommendedCourses.length,
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(left: 25, right: 25, top: 30),
                                    child: Text(
                                      "Subjects",
                                      style: GoogleFonts.roboto(
                                          fontSize: (width / 20).ceil().toDouble(), color: Colors.black87, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                    height: 150,
                                    width: MediaQuery.of(context).size.width,
                                    child: new StaggeredGridView.countBuilder(
                                      scrollDirection: Axis.horizontal,
                                      crossAxisCount: 2,
                                      itemCount: allSubjects.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).push(
                                              SlideRightRoute(
                                                page: SubjectPage(
                                                  subjectIndex: allSubjects[index].id,
                                                ),
                                              ),
                                            );
                                          },
                                          child: Container(
                                            margin: EdgeInsets.all(7.5),
                                            padding: EdgeInsets.symmetric(horizontal: 10),
                                            color: Colors.white,
                                            child: Center(
                                              child: Text(
                                                allSubjects[index].name,
                                                style: GoogleFonts.roboto(color: Colors.black, fontWeight: FontWeight.w700),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                      staggeredTileBuilder: (int index) =>
                                          new StaggeredTile.count(1, (allSubjects[index].name.length.toDouble() / 9) + 0.5),
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(left: 25, right: 25, top: 30),
                                    child: Text(
                                      "Most Popular Courses",
                                      style: GoogleFonts.roboto(
                                          fontSize: (width / 20).ceil().toDouble(), color: Colors.black87, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    height: 325,
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, i) {
                                        return CourseTile(
                                          width: width,
                                          isLast: i == popularCourses.length - 1 ? true : false,
                                          tileIndex: i,
                                          course: popularCourses[i],
                                        );
                                      },
                                      itemCount: popularCourses.length,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  // Video(),
                                ],
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          : Container(),
    );
  }

  Future<void> _readyExplorePage() async {
    EasyLoading.show(status: "Loading recommendations...");
    await _setCourses();

    setState(() {
      isLoading = false;
    });
    EasyLoading.dismiss();
  }

  Future<void> _setCourses() async {
    final res = await Provider.of<SubjectsState>(context, listen: false).getExploreData(context);
    recommendedCourses = res['recommended'];
    allSubjects = res['subjects'];
    Provider.of<SubjectsState>(context, listen: false).allSubjects = allSubjects;
    popularCourses = recommendedCourses.reversed.toList();
    popularCourses.sort((a, b) => a.nbStudentsEnrolled.compareTo(b.nbStudentsEnrolled));
    popularCourses = popularCourses.reversed.toList();
  }

  Future<void> _search(String searchValue) async {
    EasyLoading.show(status: "Searching...");
    setState(() {
      isLoading = true;
    });
    Map<String, dynamic> res = await Provider.of<SearchState>(context, listen: false).getExploreSearchData(searchValue, context);
    searchSubjects = res["subjects"];
    searchCourses = res["courses"];

    setState(() {
      isLoading = false;
    });
    EasyLoading.dismiss();
  }
}

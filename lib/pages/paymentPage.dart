import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/models/courseModel.dart';
import 'package:upSkill/states/coursesState.dart';
import 'package:upSkill/widgets/signingField.dart';

class PaymentPage extends StatelessWidget {
  final TextEditingController nameOnCardController = new TextEditingController();
  final TextEditingController cardNumberController = new TextEditingController();
  final TextEditingController expiryDateController = new TextEditingController();
  final TextEditingController securityCodeController = new TextEditingController();
  final Function buyCourse;
  final Course course;
  final _formKey = GlobalKey<FormState>();

  PaymentPage({this.buyCourse, this.course});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.arrow_back_ios_sharp,
                    color: Colors.grey,
                    size: 12,
                  ),
                  Text(
                    "Back",
                    style: GoogleFonts.roboto(color: Colors.grey, fontSize: 12),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "Buy this Course",
              style: GoogleFonts.roboto(color: Colors.black87, fontWeight: FontWeight.w700, fontSize: 20),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Container(
          height: height - 80,
          child: Padding(
            padding: EdgeInsets.all(30),
            child: Center(
              child: Column(
                children: [
                  Divider(
                    height: 30,
                  ),
                  Text(
                    course.name,
                    style: GoogleFonts.roboto(
                      fontWeight: FontWeight.bold,
                      fontSize: (width / 25).floor().toDouble(),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "By ${course.instructor.name}",
                    style: GoogleFonts.roboto(
                      color: Colors.grey,
                      fontSize: (width / 27).floor().toDouble(),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      gradient: LinearGradient(
                        colors: [
                          Color(0xFF26B7ED),
                          Color(0xFF6A80E4),
                        ],
                      ),
                    ),
                    child: Text(
                      "\$20.99 USD",
                      style: GoogleFonts.roboto(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: (width / 23).floor().toDouble(),
                      ),
                    ),
                  ),
                  Divider(
                    height: 30,
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Text(
                          "Payment Information",
                          style: GoogleFonts.roboto(
                            fontWeight: FontWeight.bold,
                            fontSize: (width / 15).floor().toDouble(),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Insert your payment method",
                          style: GoogleFonts.roboto(
                            fontWeight: FontWeight.bold,
                            fontSize: (width / 26).floor().toDouble(),
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              SigningField(
                                fieldName: "Name on Card",
                                fieldController: nameOnCardController,
                                keyboardType: TextInputType.name,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              SigningField(
                                fieldName: "Card Number",
                                fieldController: cardNumberController,
                                keyboardType: TextInputType.number,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              SigningField(
                                fieldName: "Exp. Date",
                                fieldController: expiryDateController,
                                keyboardType: TextInputType.numberWithOptions(),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              SigningField(
                                fieldName: "Security Code",
                                fieldController: securityCodeController,
                                keyboardType: TextInputType.number,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        GestureDetector(
                          onTap: () {
                            subscribeCourse(context);
                          },
                          child: Container(
                            padding: EdgeInsets.fromLTRB(60, 10, 60, 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              gradient: LinearGradient(
                                colors: [
                                  Color(0xFF6A80E4),
                                  Color(0xFF26B7ED),
                                ],
                              ),
                            ),
                            child: Text(
                              "Buy",
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: (width / 23).floor().toDouble(),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> subscribeCourse(context) async {
    bool valid = _formKey.currentState.validate();
    if (valid) {
      EasyLoading.show(status: "Subscribing...");
      final res = await Provider.of<CoursesState>(context, listen: false).subscribeCourse(course.id);
      if (res['error'] == false) {
        EasyLoading.dismiss();
        buyCourse();
        Navigator.pop(context);
      } else {
        EasyLoading.dismiss();
        return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text("Something went wrong"),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'okay',
                  style: TextStyle(
                    color: Theme.of(context).secondaryHeaderColor,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        );
      }
    }
  }
}

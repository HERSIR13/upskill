import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/models/courseModel.dart';
import 'package:upSkill/models/subjectModel.dart';
import 'package:upSkill/states/subjectsState.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';
import 'package:upSkill/widgets/courseTile.dart';
import 'package:upSkill/widgets/endDrawer.dart';
import 'package:upSkill/widgets/logoAppBar.dart';
import 'package:upSkill/widgets/video.dart';
import 'package:video_player/video_player.dart';

class SubjectPage extends StatefulWidget {
  final subjectIndex;

  SubjectPage({this.subjectIndex});

  @override
  _SubjectPageState createState() => _SubjectPageState();
}

class _SubjectPageState extends State<SubjectPage> {
  bool isLoading = true;
  Subject subjectData;
  List<Course> beginnerCourses = [];
  List<Course> advancedCourses = [];

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String videoUrl = "";
  VideoPlayerController _videoController;

  bool isGuest = true;

  void initializeVideo(videoUrl) {
    _videoController = VideoPlayerController.network(videoUrl)
      ..initialize().then((_) {
        setState(() {});
      });
    _videoController.setLooping(true);
  }

  @override
  void initState() {
    _getGuestStatus();
    _getSubjectData(context);
    super.initState();
  }

  Future<void> _getGuestStatus() async {
    isGuest = await SharedPreferencesHelper.getGuestStatus();
    if (isGuest == false) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Consumer<SubjectsState>(
      builder: (context, subjects, _) => Scaffold(
        key: _scaffoldKey,
        endDrawer: isGuest ? null : CustomEndDrawer(),
        appBar: isGuest
            ? LogoAppBar.appBar
            : AppBar(
                toolbarHeight: 60,
                backgroundColor: Colors.white,
                automaticallyImplyLeading: false,
                title: Text(
                  "Courses",
                  style: GoogleFonts.roboto(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 25),
                ),
                actions: [
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: IconButton(
                      onPressed: () => _scaffoldKey.currentState.openEndDrawer(),
                      icon: Icon(
                        Icons.menu,
                        color: Colors.grey[600],
                        size: 30,
                      ),
                    ),
                  ),
                ],
              ),
        backgroundColor: Color(0xFFF0F0F0),
        body: !isLoading
            ? SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 30),
                      color: Colors.white,
                      child: Column(
                        children: [
                          Center(
                            child: Column(
                              children: [
                                Text(
                                  "Discover Best Online",
                                  style: GoogleFonts.roboto(
                                    fontSize: (width / 25).floor().toDouble(),
                                    color: Colors.black54,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "${subjectData.name} Courses",
                                  style: GoogleFonts.roboto(
                                    fontSize: (width / 17).floor().toDouble(),
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Video(
                                  controller: _videoController,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 25, right: 25, top: 30),
                      child: Text(
                        "Beginner Courses",
                        style: GoogleFonts.roboto(fontSize: (width / 20).floor().toDouble(), color: Colors.black54, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 325,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, i) {
                          return CourseTile(
                            width: width,
                            isLast: i == beginnerCourses.length - 1 ? true : false,
                            tileIndex: i,
                            course: beginnerCourses[i],
                          );
                        },
                        itemCount: beginnerCourses.length,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 25, right: 25, top: 30),
                      child: Text(
                        "Advanced Courses",
                        style: GoogleFonts.roboto(fontSize: (width / 20).floor().toDouble(), color: Colors.black54, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 325,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, i) {
                          return CourseTile(
                            width: width,
                            isLast: i == advancedCourses.length - 1 ? true : false,
                            tileIndex: i,
                            course: advancedCourses[i],
                          );
                        },
                        itemCount: advancedCourses.length,
                      ),
                    ),
                    SizedBox(
                      height: height / 20,
                    ),
                  ],
                ),
              )
            : Container(),
      ),
    );
  }

  Future<void> _getSubjectData(context) async {
    EasyLoading.show(status: "Loading subject...");
    subjectData = await Provider.of<SubjectsState>(context, listen: false).getSubjectData(context, widget.subjectIndex);

    if (subjectData != null) {
      subjectData.courses.forEach((course) {
        if (course.level == "beginner")
          beginnerCourses.add(course);
        else
          advancedCourses.add(course);
      });
      videoUrl = subjectData.introductoryVideo;
      initializeVideo(videoUrl);
    }
    setState(() {
      isLoading = false;
    });
    EasyLoading.dismiss();
  }
}

//Karim's version
import 'package:flutter/material.dart';
import 'package:upSkill/widgets/logoAppBar.dart';

class AboutUsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LogoAppBar.appBar,
      body:
          //---
          Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Card(
                  elevation: 5,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    child: Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerRight,
                          child: Stack(
                            children: <Widget>[
                              Column(
                                children: [
                                  Text(
                                    "Our Mission",
                                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Text(
                                    "We envision a world where anyone, anywhere can transform their life by accessing the world’s best learning experience.\nWe empower our learners to advance their careers, further their studies,improve their communities, and change the world.\nWe believe education can unlock your potential and help you becomeyour best self.",
                                    style: TextStyle(fontSize: 16, color: Colors.grey[600]),
                                  )
                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 5,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    child: Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerRight,
                          child: Stack(
                            children: <Widget>[
                              Column(
                                children: [
                                  Text(
                                    "Our Vision",
                                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Text(
                                    "UpSkill was founded in 2020 by 8 RHU students with a vision to bring quality skill building content and world class learning experience to everyone using both online and offline mediums.",
                                    style: TextStyle(fontSize: 16, color: Colors.grey[600]),
                                  )
                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 5,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    child: Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerRight,
                          child: Stack(
                            children: <Widget>[
                              Column(
                                children: [
                                  Text(
                                    "''Founder's Quote, we are dedicated to deliver the highest quality education experience''",
                                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),
                                  ),
                                ],
                                crossAxisAlignment: CrossAxisAlignment.center,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

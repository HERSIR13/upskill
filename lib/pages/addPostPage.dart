import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/states/postsState.dart';

class AddPostPage extends StatefulWidget {
  final bool isComment;
  final int postId;
  final int subjectId;
  final Function refreshPost;

  AddPostPage({this.isComment, this.postId, this.subjectId, this.refreshPost});

  @override
  _AddPostPageState createState() => _AddPostPageState();
}

class _AddPostPageState extends State<AddPostPage> {
  String postTitle = "";
  String postDescription = "";
  final ImagePicker _picker = ImagePicker();
  Future<File> file;
  File _image;
  final _formKey = GlobalKey<FormState>();

  Future getImage(bool camera) async {
    var image = await _picker.getImage(source: camera ? ImageSource.camera : ImageSource.gallery, imageQuality: 100, maxWidth: 150);

    if (image != null) {
      setState(() {
        _image = File(image.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.fromLTRB(30, 60, 30, 0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                        addPost(context);
                      },
                      child: Container(
                        height: 35,
                        width: MediaQuery.of(context).size.width / 3.5,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          gradient: LinearGradient(
                            colors: [
                              Color(0xFF6380E4),
                              Color(0xFFA547DA),
                            ],
                          ),
                        ),
                        child: Center(
                            child: Text(
                          "Submit",
                          style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white),
                        )),
                      ),
                    ),
                  ],
                ),
                if (!widget.isComment)
                  Column(
                    children: [
                      editTitleTextField(),
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                editDescriptionTextField(),
                SizedBox(
                  height: 30,
                ),
                InkWell(
                  onTap: () {
                    FocusScope.of(context).unfocus();
                    return showDialog(
                      context: context,
                      builder: (ctx) => AlertDialog(
                        title: Text('Choose Source'),
                        actions: [
                          TextButton(
                            onPressed: () {
                              getImage(false);
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              'Gallery',
                              style: TextStyle(
                                color: Color(0xFF6A80E4),
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              getImage(true);
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              'Camera',
                              style: TextStyle(
                                color: Color(0xFF6A80E4),
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                  child: Center(
                    child: _image != null
                        ? Image(
                            image: FileImage(_image),
                          )
                        : Container(
                            height: 40,
                            color: Colors.grey,
                            child: Center(
                              child: Icon(Icons.camera_alt_outlined),
                            ),
                          ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget editTitleTextField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Title",
          style: GoogleFonts.roboto(fontSize: 20),
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          textAlign: TextAlign.start,
          maxLines: 1,
          validator: (val) {
            if (val.isEmpty && !widget.isComment) {
              return "This field should not be empty";
            }
            return null;
          },
          onChanged: (newValue) {
            postTitle = newValue;
          },
          decoration: InputDecoration(
            alignLabelWithHint: true,
            labelText: widget.isComment ? "Comment Title" : "Post Title",
            labelStyle: GoogleFonts.roboto(
              fontSize: 18,
            ),
            border: OutlineInputBorder(),
            filled: true,
            fillColor: Color(0xfff3f3f3),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Theme.of(context).accentColor,
                width: 2.0,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xFF6380E4),
                width: 2.0,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget editDescriptionTextField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Description",
          style: GoogleFonts.roboto(fontSize: 20),
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          textAlign: TextAlign.start,
          maxLines: 5,
          validator: (val) {
            if (val.isEmpty && widget.isComment) {
              return "This field should not be empty";
            }
            return null;
          },
          onChanged: (newValue) {
            postDescription = newValue;
          },
          decoration: InputDecoration(
            alignLabelWithHint: true,
            labelText: widget.isComment ? "Comment Description" : "Post Description",
            labelStyle: GoogleFonts.roboto(
              fontSize: 15,
            ),
            border: OutlineInputBorder(),
            filled: true,
            fillColor: Color(0xfff3f3f3),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Theme.of(context).accentColor,
                width: 2.0,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xFF6380E4),
                width: 2.0,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> addPost(context) async {
    bool valid = _formKey.currentState.validate();
    if (valid) {
      await EasyLoading.show(status: "Posting...");
      final res = await Provider.of<PostsState>(context, listen: false).storePostOrComment(
          context, widget.isComment ? widget.postId : widget.subjectId, postTitle, postDescription,
          image: _image, isComment: widget.isComment ? true : false);
      if (res['error'] == false) {
        await EasyLoading.dismiss();
        Navigator.pop(context);
        widget.refreshPost();
      } else {
        EasyLoading.dismiss();
        return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text("Something went wrong"),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'okay',
                  style: TextStyle(
                    color: Theme.of(context).secondaryHeaderColor,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        );
      }
    }
  }
}

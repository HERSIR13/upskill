import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/models/subjectModel.dart';
import 'package:upSkill/states/profileState.dart';
import 'package:upSkill/states/subjectsState.dart';
import 'package:upSkill/widgets/InterestTile.dart';
import 'package:upSkill/widgets/mainLayout.dart';

class InterestsPage extends StatefulWidget {
  final isEditProfile;
  final Function setNewInterests;

  InterestsPage({this.isEditProfile, this.setNewInterests});

  @override
  _InterestsPageState createState() => _InterestsPageState();
}

class _InterestsPageState extends State<InterestsPage> {
  final List<String> myInterests = [];
  bool _isLoading = true;
  List<Subject> subjects = [];
  Random random = new Random();

  @override
  void initState() {
    _readyExplorePage();
    super.initState();
  }

  void updateInterests(String interest, bool add) {
    if (add)
      myInterests.add(interest);
    else
      myInterests.removeWhere((toBeRemovedInterest) => toBeRemovedInterest == interest);
    myInterests.sort((a, b) => a.compareTo(b));
  }

  List<LinearGradient> gradientsList = [
    LinearGradient(
      colors: [
        Color(0xFF6380E4),
        Color(0xFFA547DA),
      ],
    ),
    LinearGradient(
      colors: [
        Color(0xFFF400CF),
        Color(0xFFA547DA),
      ],
    ),
    LinearGradient(
      colors: [
        Color(0xFF6A80E4),
        Color(0xFF26B7ED),
      ],
    ),
  ];

  void _onCheckMarkerPressed() async {
    setState(() {
      _isLoading = true;
    });
    if (widget.isEditProfile == null) {
      await EasyLoading.show(status: "Updating...");
      String finalInterests = myInterests.join(",");
      await Provider.of<ProfileState>(context, listen: false).updateProfile(interests: finalInterests);

      await EasyLoading.dismiss();
      setState(() {
        _isLoading = false;
      });
      Navigator.of(context).pushReplacement(
        SlideRightRoute(
          page: MainPageLayout(),
        ),
      );
    } else {
      await EasyLoading.show(status: "Updating...");
      String finalInterests = myInterests.join(",");
      widget.setNewInterests(
        finalInterests,
      );
      Navigator.pop(context);
      await EasyLoading.dismiss();
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // backgroundColor: Colors.grey[300],
      appBar: AppBar(
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        title: Text(
          "Interests",
          style: GoogleFonts.roboto(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 25),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              if (myInterests.length >= 1) {
                _onCheckMarkerPressed();
              } else
                return showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                    title: Text(
                      "Please choose at least one interest",
                      style: GoogleFonts.roboto(fontSize: 20),
                    ),
                    content: Text("Currently selected: ${myInterests.length} ${myInterests.length != 1 ? "subjects" : "subject"}"),
                    actions: [
                      TextButton(
                        child: Text(
                          "Ok",
                          style: GoogleFonts.roboto(color: Colors.black87),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                );
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: Icon(
                Icons.check,
                color: Colors.black,
                size: 30,
              ),
            ),
          )
        ],
      ),
      body: !_isLoading
          ? Container(
              margin: EdgeInsets.all(5),
              child: GridView.count(
                childAspectRatio: 1.5,
                primary: false,
                crossAxisCount: 2,
                mainAxisSpacing: 10,
                crossAxisSpacing: 5,
                children: List.generate(subjects.length, (index) {
                  int randomNumber = random.nextInt(2);
                  return Container(
                    child: Interest(
                      interestName: subjects[index].name,
                      interestImage: subjects[index].image,
                      interestGradient: gradientsList[randomNumber],
                      updateInterestsList: updateInterests,
                    ),
                  );
                }),
              ),
            )
          : Container(),
    );
  }

  Future<void> _readyExplorePage() async {
    EasyLoading.show(status: "Loading things you might be interested in...");
    await _setSubjects();

    setState(() {
      _isLoading = false;
    });
    EasyLoading.dismiss();
  }

  Future<void> _setSubjects() async {
    await Provider.of<SubjectsState>(context, listen: false).setSubjects(context);
    subjects = Provider.of<SubjectsState>(context, listen: false).allSubjects;
  }
}

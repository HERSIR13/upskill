import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/states/authenticationState.dart';
import 'package:upSkill/states/profileState.dart';
import 'package:upSkill/widgets/mainLayout.dart';
import 'package:flutter/services.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  Timer timer;

  Future checkAuthentication(BuildContext context) async {
    String _authToken = await SharedPreferencesHelper.getAuthToken();
    Map<String, dynamic> loginInfo = await SharedPreferencesHelper.getEmailPassword();

    if (_authToken == '' && loginInfo["email"] == '' && loginInfo["password"] == '') {
      final response = await Provider.of<AuthenticationState>(context, listen: false).guestLogin();
      Provider.of<ProfileState>(context, listen: false).setProfile(response["response"], context, isGuest: true);
      Navigator.of(context).pushReplacement(
        SlideRightRoute(
          page: MainPageLayout(),
        ),
      );
    } else {
      if (loginInfo["email"] != '' && loginInfo["password"] != '') {
        final response =
            await Provider.of<AuthenticationState>(context, listen: false).login(email: loginInfo["email"], password: loginInfo["password"]);
        Provider.of<ProfileState>(context, listen: false).setProfile(response["response"], context, isGuest: false);
        Navigator.of(context).pushReplacement(
          SlideRightRoute(
            page: MainPageLayout(),
          ),
        );
      } else {
        final response = await Provider.of<AuthenticationState>(context, listen: false).guestLogin();
        Provider.of<ProfileState>(context, listen: false).setProfile(response["response"], context, isGuest: true);
        Navigator.of(context).pushReplacement(
          SlideRightRoute(
            page: MainPageLayout(),
          ),
        );
      }
    }
  }

  @override
  void initState() {
    timer = Timer(const Duration(seconds: 1, milliseconds: 500), () {
      checkAuthentication(context);
    });
    super.initState();
  }

  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: Scaffold(
        body: Center(
          child: Image.asset(
            "assets/images/Logo.png",
            scale: 2.0,
          ),
        ),
      ),
    );
  }
}

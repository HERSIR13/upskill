import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:upSkill/models/instructorModel.dart';
import 'package:upSkill/widgets/video.dart';
import 'package:video_player/video_player.dart';

class InstructorPage extends StatelessWidget {
  final Instructor instructor;

  InstructorPage({this.instructor});


  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final _videoController = VideoPlayerController.network(instructor.introductoryVideo)..initialize().then((_) {});
    _videoController.setLooping(true);
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        title: Text(
          "Instructor's Page",
          style: GoogleFonts.roboto(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 25),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: Icon(
              Icons.menu,
              color: Colors.grey[600],
              size: 30,
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Video(
              controller: _videoController,
            ),
            Container(
              margin: const EdgeInsets.all(30.0),
              child: new LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  return Column(
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                            foregroundImage: NetworkImage(
                              instructor.avatar,
                            ),
                            minRadius: 55,
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Flexible(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ShaderMask(
                                  shaderCallback: (bounds) => LinearGradient(colors: [
                                    Color(0xFFF400CF),
                                    Color(0xFFA547DA),
                                  ]).createShader(
                                    Rect.fromLTWH(0, 0, bounds.width, bounds.height),
                                  ),
                                  child: Text(
                                    instructor.name,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: (width / 15).floor().toDouble(),
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Text(
                                  instructor.job,
                                  style: GoogleFonts.roboto(
                                      fontSize: (width / 30).floor().toDouble(), color: Colors.black54, fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.person_outline_outlined,
                            size: 30,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 10),
                            width: constraints.maxWidth - 60,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Biography",
                                  style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: (width / 20).floor().toDouble()),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  instructor.biography ??
                                      "Elon Musk co-founded and leads Tesla, SpaceX, Neuralink and The Boring Company. As the co-founder and CEO of Tesla, Elon leads all product design, engineering and global manufacturing of the company's electric vehicles, battery products and solar energy products.",
                                  style: GoogleFonts.roboto(fontSize: (width / 30).floor().toDouble(), color: Colors.grey[500]),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.thumb_up_alt_outlined,
                            size: 30,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 10),
                            width: constraints.maxWidth - 60,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Achievements",
                                  style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: (width / 20).floor().toDouble()),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  instructor.achievements ??
                                      "Board member and the Administrator of Formalities Commission at the Lebanese Graphic Design Syndicate.\n\n Member in the Lebanese Graphic Design Syndicate.",
                                  style: GoogleFonts.roboto(fontSize: (width / 30).floor().toDouble(), color: Colors.grey[500]),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.insert_drive_file_outlined,
                            size: 30,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 10),
                            width: constraints.maxWidth - 60,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Education",
                                  style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: (width / 20).floor().toDouble()),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  instructor.education ??
                                      "BS in Graphic Design - Lebanese American University-LAU, Faculty of Arts and Science- Emphasis: Print and Digital Media. Beirut, Lebanon, 2007.",
                                  style: GoogleFonts.roboto(fontSize: (width / 30).floor().toDouble(), color: Colors.grey[500]),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.local_movies_outlined,
                            size: 30,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 10),
                            width: constraints.maxWidth - 60,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Professional Interests",
                                  style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: (width / 20).floor().toDouble()),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  instructor.interests ?? "philanthropist, tech geek, into electrical cars.",
                                  style: GoogleFonts.roboto(fontSize: (width / 30).floor().toDouble(), color: Colors.grey[500]),
                                )
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

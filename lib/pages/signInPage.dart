import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/pages/interestsPage.dart';
import 'package:upSkill/states/authenticationState.dart';
import 'package:upSkill/states/profileState.dart';
import 'package:upSkill/widgets/logoAppBar.dart';
import 'package:upSkill/widgets/mainLayout.dart';
import 'package:upSkill/widgets/signingField.dart';

class SignInPage extends StatefulWidget {
  final bool isSignUp;

  SignInPage({this.isSignUp});

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController confirmPasswordController = new TextEditingController();

  bool isSignUp;
  final _formKey = GlobalKey<FormState>();

  bool checkPasswords() {
    if (passwordController.text == confirmPasswordController.text)
      return true;
    else
      return false;
  }

  bool validateInput() {
    final isValid = _formKey.currentState.validate();
    FocusScope.of(context).unfocus();
    return isValid;
  }

  void _onLoginPress() async {
    final _isValid = validateInput();
    if (!_isValid) {
      return;
    }
    await EasyLoading.show(status: "logging in...");
    final auth = Provider.of<AuthenticationState>(context, listen: false);
    Map response = await auth.login(email: emailController.text, password: passwordController.text);
    await EasyLoading.dismiss();
    if (response['error']) {
      showErrorDialog(response);
    } else {
      Provider.of<ProfileState>(context, listen: false).setProfile(response["response"], context);
      Navigator.of(context).pushReplacement(
        SlideRightRoute(
          page: MainPageLayout(),
        ),
      );
    }
  }

  @override
  void initState() {
    isSignUp = widget.isSignUp;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LogoAppBar.appBar,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) => SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  margin: EdgeInsets.only(top: 30),
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 300,
                            child: Column(
                              children: [
                                Text(
                                  isSignUp ? "Create An Account" : "Sign In",
                                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                                ),
                                if (isSignUp)
                                  Column(
                                    children: [
                                      SigningField(
                                        fieldName: "Username",
                                        fieldController: nameController,
                                        keyboardType: TextInputType.name,
                                      ),
                                    ],
                                  ),
                                SigningField(
                                  fieldName: "Email",
                                  fieldController: emailController,
                                  keyboardType: TextInputType.emailAddress,
                                ),
                                SigningField(
                                  fieldName: "Password",
                                  fieldController: passwordController,
                                  keyboardType: TextInputType.visiblePassword,
                                ),
                                if (isSignUp)
                                  SigningField(
                                    fieldName: "Confirm Password",
                                    fieldController: confirmPasswordController,
                                    checkPasswords: checkPasswords,
                                    keyboardType: TextInputType.visiblePassword,
                                  ),
                                if (!isSignUp)
                                  Column(
                                    children: [
                                      Container(
                                        height: 30,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            TextButton(
                                              style: ButtonStyle(padding: MaterialStateProperty.all(EdgeInsets.zero)),
                                              onPressed: () {},
                                              child: Text(
                                                "Forgot Password?",
                                                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14, color: Colors.grey[600]),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 30,
                                      ),
                                      Container(
                                        width: MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                          color: Colors.grey[300],
                                          borderRadius: BorderRadius.circular(30),
                                        ),
                                        padding: EdgeInsets.all(8),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Image(
                                              image: AssetImage('assets/images/gmailIcon.png'),
                                              width: 20,
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(
                                              "Sign In with Google",
                                              style: GoogleFonts.roboto(color: Colors.grey[800], fontWeight: FontWeight.w500, fontSize: 13),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Container(
                                        width: MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                          color: Colors.grey[300],
                                          borderRadius: BorderRadius.circular(30),
                                        ),
                                        padding: EdgeInsets.all(8),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Image(
                                              image: AssetImage('assets/images/fb.png'),
                                              width: 20,
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(
                                              "Sign In with Facebook",
                                              style: GoogleFonts.roboto(color: Colors.grey[800], fontWeight: FontWeight.w500, fontSize: 13),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                SizedBox(
                                  height: 30,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    if (isSignUp) {
                                      final isValid = validateInput();
                                      if (isValid) {
                                        _onJoinNowButtonPressed();
                                      }
                                    } else {
                                      _onLoginPress();
                                    }
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width / 2.5,
                                    decoration: BoxDecoration(
                                      color: Colors.deepPurpleAccent,
                                      borderRadius: BorderRadius.circular(30),
                                      gradient: LinearGradient(
                                        colors: [
                                          Color(0xFF8564DF),
                                          Color(0xFFA547DA),
                                        ],
                                      ),
                                    ),
                                    padding: EdgeInsets.all(8),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          isSignUp ? "Join Now" : "Sign In",
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 40,
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    Color(0xFF6380E4),
                                    Color(0xFFA547DA),
                                  ],
                                ),
                              ),
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 30.0),
                                  child: Column(
                                    children: [
                                      Text(
                                        isSignUp ? "Already a Member?" : "New to UpSkill?",
                                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 27, color: Colors.white),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            isSignUp = !isSignUp;
                                          });
                                        },
                                        child: Container(
                                          height: 35,
                                          width: MediaQuery.of(context).size.width / 3.5,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(25),
                                          ),
                                          padding: EdgeInsets.all(5),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                isSignUp ? "Sign In" : "Sign Up",
                                                style: GoogleFonts.roboto(color: Color(0xFF707070), fontWeight: FontWeight.w700, fontSize: 17),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              childCount: 1,
            ),
          ),
        ],
      ),
    );
  }

  Future<void> showErrorDialog(Map data) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Invalid data'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(isSignUp ? 'Email Already Taken' : 'Email or password is incorrect'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _onJoinNowButtonPressed() async {
    await EasyLoading.show(status: "Registering...");
    Map response = await Provider.of<AuthenticationState>(context, listen: false).register(
      name: nameController.text,
      email: emailController.text,
      password: passwordController.text,
    );
    await EasyLoading.dismiss();
    if (response['error']) {
      showErrorDialog(response);
    } else {
      Provider.of<ProfileState>(context, listen: false).setProfile(response["response"], context);
      Navigator.of(context).pushReplacement(
        SlideRightRoute(
          page: InterestsPage(),
        ),
      );
    }
  }
}

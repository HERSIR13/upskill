import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/models/courseModel.dart';
import 'package:upSkill/models/reviewModel.dart';
import 'package:upSkill/pages/addReviewPage.dart';
import 'package:upSkill/pages/paymentPage.dart';
import 'package:upSkill/states/coursesState.dart';
import 'package:upSkill/states/profileState.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';
import 'package:upSkill/widgets/courseLecturesExpansionTile.dart';
import 'package:upSkill/widgets/courseObjectivesExpansionTile.dart';
import 'package:upSkill/widgets/courseReviewsExpansionTile.dart';
import 'package:upSkill/widgets/courseTile.dart';
import 'package:upSkill/widgets/descriptionExpansionTile.dart';
import 'package:upSkill/widgets/endDrawer.dart';
import 'package:upSkill/widgets/logoAppBar.dart';
import 'package:upSkill/widgets/video.dart';
import 'package:upSkill/pages/signInPage.dart';
import 'package:video_player/video_player.dart';
import 'instructorPage.dart';

class CoursePage extends StatefulWidget {
  final int courseId;
  final bool fromLatestActivity;
  final int lectureId;

  CoursePage({this.courseId, this.fromLatestActivity, this.lectureId});

  @override
  _CoursePageState createState() => _CoursePageState();
}

class _CoursePageState extends State<CoursePage> {
  bool isBought;
  bool isLectureChosen = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = true;
  Course course = new Course();
  List<Course> recommendedCourses = [];
  List<Review> reviews = [];
  String videoUrl = "";
  VideoPlayerController _videoController;
  int currentLectureIndex = -1;
  bool isGuest = true;
  VideoProgressIndicator vPI;

  void initializeVideo(videoUrl) {
    if (_videoController != null)
      setState(() {
        _videoController.value = null;
      });
    _videoController = VideoPlayerController.network(videoUrl)
      ..initialize().then((_) {
        setState(() {});
      });

    _videoController.setLooping(true);
  }

  List<dynamic> courseObjectives = [];

  @override
  void initState() {
    _getGuestStatus();
    _setCourseData();
    _checkIfSubscribed();
    super.initState();
  }

  Future<void> _getGuestStatus() async {
    isGuest = await SharedPreferencesHelper.getGuestStatus();
    if (isGuest == false) setState(() {});
  }

  Future<void> setIsLectureChosen(bool isNewLectureChosen, {int lectureIndex}) async {
    if (isNewLectureChosen && currentLectureIndex != lectureIndex - 1) {
      isLectureChosen = true;
      if (isLectureChosen) {
        currentLectureIndex = lectureIndex - 1;
        videoUrl = course.lectures[lectureIndex - 1].video;
        initializeVideo(videoUrl);
        await SharedPreferencesHelper.setLatestActivity(
          courseId: course.id,
          courseName: course.name,
          courseImage: course.image,
          lectureId: lectureIndex,
          lectureName: course.lectures[lectureIndex - 1].name,
        );
      }
      setState(() {});
    } else if (!isNewLectureChosen) {
      isLectureChosen = false;
      currentLectureIndex = -1;
      videoUrl = course.introductoryVideo;
      initializeVideo(videoUrl);
      setState(() {});
    }
  }

  void buyCourse() {
    setState(() {
      Provider.of<ProfileState>(context, listen: false).profile.subscribedCourses.add(course);
      isBought = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      endDrawer: isGuest ? null : CustomEndDrawer(),
      appBar: !isGuest
          ? AppBar(
              toolbarHeight: 60,
              backgroundColor: Colors.white,
              automaticallyImplyLeading: false,
              title: Text(
                "Course Page",
                style: GoogleFonts.roboto(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 25),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: IconButton(
                    onPressed: () => _scaffoldKey.currentState.openEndDrawer(),
                    icon: Icon(
                      Icons.menu,
                      color: Colors.grey[600],
                      size: 30,
                    ),
                  ),
                ),
              ],
            )
          : LogoAppBar.appBar,
      body: !isLoading
          ? Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(25, 30, 25, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              course.name,
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: (width / 20).floor().toDouble(),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "By ${course.instructor.name}",
                              style: GoogleFonts.roboto(
                                color: Colors.grey,
                                fontSize: (width / 27).floor().toDouble(),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      if (_videoController.value.isInitialized)
                        Video(
                          controller: _videoController,
                          vPI: vPI,
                        ),
                      Column(
                        children: [
                          if (!isBought)
                            Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(25, 15, 25, 15),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      if (course.isBestSeller)
                                        Container(
                                          padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(20),
                                            gradient: LinearGradient(
                                              colors: [
                                                Color(0xFF6A80E4),
                                                Color(0xFF26B7ED),
                                              ],
                                            ),
                                          ),
                                          child: Text(
                                            "Best Seller",
                                            style: GoogleFonts.roboto(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: (width / 20).floor().toDouble(),
                                            ),
                                          ),
                                        ),
                                      if (course.isNew && !course.isBestSeller)
                                        Container(
                                          padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(20),
                                            gradient: course.isBestSeller
                                                ? LinearGradient(
                                                    colors: [
                                                      Color(0xFF6A80E4),
                                                      Color(0xFF26B7ED),
                                                    ],
                                                  )
                                                : LinearGradient(
                                                    colors: [
                                                      Color(0xFFF400CF),
                                                      Color(0xFFA547DA),
                                                    ],
                                                  ),
                                          ),
                                          child: Text(
                                            "New course",
                                            style: GoogleFonts.roboto(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: (width / 20).floor().toDouble(),
                                            ),
                                          ),
                                        ),
                                      Container(
                                        height: 25,
                                        width: 110,
                                        child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          shrinkWrap: true,
                                          itemBuilder: (context, j) {
                                            return Icon(
                                              j + 1 <= course.rating ? Icons.star : Icons.star_border,
                                              size: 22,
                                              color: j + 1 <= course.rating ? Colors.amberAccent : Colors.grey,
                                            );
                                          },
                                          itemCount: 5,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () => Navigator.of(context).push(
                                    SlideRightRoute(
                                      page: InstructorPage(
                                        instructor: course.instructor,
                                      ),
                                    ),
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.all(20),
                                    color: Colors.grey[200],
                                    width: width,
                                    height: 150,
                                    child: Row(
                                      children: [
                                        CircleAvatar(
                                          foregroundImage: NetworkImage(
                                            course.instructor.avatar,
                                          ),
                                          minRadius: 55,
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        Flexible(
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                course.instructor.name,
                                                style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontSize: (width / 20).floor().toDouble(),
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              Text(
                                                course.instructor.job,
                                                style: GoogleFonts.roboto(
                                                    fontSize: (width / 30).floor().toDouble(), color: Colors.black54, fontWeight: FontWeight.bold),
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    "view instructor's page",
                                                    style: GoogleFonts.roboto(
                                                      color: Colors.grey[600],
                                                      fontSize: 12,
                                                    ),
                                                  ),
                                                  Icon(
                                                    Icons.arrow_forward_ios,
                                                    color: Colors.grey[600],
                                                    size: 12,
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(20),
                                  width: width,
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Icon(Icons.thumb_up_outlined),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Text(
                                            "${course.positivity}% Positive reviews",
                                            style: GoogleFonts.roboto(
                                              color: Colors.black87,
                                              fontSize: (width / 30).floor().toDouble(),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          Icon(Icons.person_outline),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Text(
                                            "${course.nbStudentsEnrolled} students enrolled",
                                            style: GoogleFonts.roboto(
                                              color: Colors.black87,
                                              fontSize: (width / 30).floor().toDouble(),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          Icon(Icons.local_movies_outlined),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Text(
                                            "${course.length} full Lessons (3h 20m)",
                                            style: GoogleFonts.roboto(
                                              color: Colors.black87,
                                              fontSize: (width / 30).floor().toDouble(),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          Icon(Icons.insert_drive_file_outlined),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Text(
                                            "Additional downloadable resources",
                                            style: GoogleFonts.roboto(
                                              color: Colors.black87,
                                              fontSize: (width / 30).floor().toDouble(),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        color: Colors.grey[200],
                        child: Column(
                          children: [
                            if (isBought)
                              Column(
                                children: [
                                  CourseLecturesExpansionTile(
                                    lectures: course.lectures,
                                    setIsLectureChosen: setIsLectureChosen,
                                    currentLectureIndex: currentLectureIndex + 1,
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              ),
                            if (!isLectureChosen)
                              Column(
                                children: [
                                  Column(
                                    children: [
                                      DescriptionExpansionTile(isCourseDescription: true, description: course.description),
                                      SizedBox(
                                        height: 20,
                                      ),
                                    ],
                                  ),
                                  CourseObjectivesExpansionTile(
                                    courseObjectives: courseObjectives,
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  CourseReviewsExpansionTile(
                                    reviews: reviews,
                                  ),
                                ],
                              ),
                            if (isLectureChosen && isBought)
                              Column(
                                children: [
                                  Column(
                                    children: [
                                      DescriptionExpansionTile(
                                          isCourseDescription: false, description: course.lectures[currentLectureIndex].description),
                                      SizedBox(
                                        height: 20,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(bottom: isBought ? 30 : 100),
                        color: Colors.grey[200],
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.only(left: 25, right: 25, top: 15),
                              child: Text(
                                "Recommended for you",
                                style:
                                    GoogleFonts.roboto(fontSize: (width / 20).ceil().toDouble(), color: Colors.black87, fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 350,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, i) => CourseTile(
                                  width: width,
                                  isLast: i == recommendedCourses.length - 1 ? true : false,
                                  tileIndex: i,
                                  course: recommendedCourses[i],
                                ),
                                itemCount: recommendedCourses.length,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                if (!isBought)
                  Positioned(
                    bottom: 15,
                    left: 10,
                    right: 10,
                    child: Container(
                      height: 60,
                      decoration: BoxDecoration(
                        boxShadow: [BoxShadow(blurRadius: 8, spreadRadius: 1, color: Colors.grey[400])],
                        color: Colors.white,
                      ),
                      padding: EdgeInsets.all(5),
                      width: width,
                      child: Row(
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () async {
                                if (!isGuest)
                                  Navigator.of(context).push(
                                    SlideRightRoute(
                                      page: PaymentPage(
                                        buyCourse: buyCourse,
                                        course: course,
                                      ),
                                    ),
                                  );
                                else
                                  showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      backgroundColor: Colors.transparent,
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                      contentPadding: EdgeInsets.zero,
                                      content: Container(
                                        height: 125,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(20),
                                          gradient: LinearGradient(
                                            colors: [
                                              Color(0xFF6380E4).withOpacity(0.9),
                                              Color(0xFFA547DA).withOpacity(0.9),
                                            ],
                                          ),
                                        ),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pushReplacement(
                                                  SlideRightRoute(
                                                    page: SignInPage(
                                                      isSignUp: true,
                                                    ),
                                                  ),
                                                );
                                              },
                                              child: Text(
                                                "Create an account?",
                                                style: GoogleFonts.roboto(fontSize: 17, color: Colors.white, fontWeight: FontWeight.bold),
                                              ),
                                            ),
                                            Divider(
                                              height: 10,
                                              thickness: 2,
                                              color: Colors.white,
                                            ),
                                            TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pushReplacement(
                                                  SlideRightRoute(
                                                    page: SignInPage(
                                                      isSignUp: false,
                                                    ),
                                                  ),
                                                );
                                              },
                                              child: Text(
                                                "Already have an account?",
                                                style: GoogleFonts.roboto(fontSize: 17, color: Colors.white, fontWeight: FontWeight.bold),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                              },
                              child: Container(
                                height: 50,
                                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      Color(0xFF6A80E4),
                                      Color(0xFF26B7ED),
                                    ],
                                  ),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(
                                      "\$${course.price} USD",
                                      style: GoogleFonts.roboto(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: (width / 20).floor().toDouble(),
                                      ),
                                    ),
                                    Text(
                                      "Buy Now",
                                      style: GoogleFonts.roboto(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: (width / 25).floor().toDouble(),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
              ],
            )
          : Container(),
      floatingActionButton: isBought
          ? FloatingActionButton(
              child: Icon(
                Icons.rate_review_rounded,
                color: Colors.black54,
              ),
              backgroundColor: Colors.white,
              onPressed: () {
                Navigator.of(context).push(
                  SlideRightRoute(
                    page: AddReviewPage(
                      courseId: course.id,
                      addReview: addReview,
                    ),
                  ),
                );
              },
            )
          : null,
    );
  }

  Future<void> _setCourseData() async {
    EasyLoading.show(status: "Loading course...");
    final res = await Provider.of<CoursesState>(context, listen: false).getCourseData(widget.courseId);
    course = res["course"];
    recommendedCourses = res["recommendedCourses"];
    reviews = course.reviews;
    courseObjectives = course.objectives;
    if (widget.fromLatestActivity == true) {
      checkChosenLecture();
    } else {
      videoUrl = course.introductoryVideo;
      initializeVideo(videoUrl);
    }
    setState(() {
      isLoading = false;
    });
    EasyLoading.dismiss();
  }

  void _checkIfSubscribed() {
    List<Course> subscribedCourses = Provider.of<ProfileState>(context, listen: false).profile.subscribedCourses;
    try {
      subscribedCourses.firstWhere((course) => course.id == widget.courseId);
      setState(() {
        isBought = true;
      });
    } catch (err) {
      setState(() {
        isBought = false;
      });
    }
  }

  void addReview(int reviewId, String title, String description, int rating) {
    Review review = new Review();
    review.id = reviewId;
    review.courseId = course.id;
    review.studentName = Provider.of<ProfileState>(context, listen: false).profile.name;
    review.title = title;
    review.rating = rating;
    review.description = description;
    setState(() {
      reviews.add(review);
    });
  }

  void checkChosenLecture() {
    if (widget.fromLatestActivity == null)
      isLectureChosen = false;
    else {
      setIsLectureChosen(true, lectureIndex: widget.lectureId);
    }
  }
}

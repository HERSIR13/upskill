import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/states/reviewsState.dart';

class AddReviewPage extends StatefulWidget {
  final int courseId;
  final Function addReview;

  AddReviewPage({this.courseId, this.addReview});

  @override
  _AddReviewPageState createState() => _AddReviewPageState();
}

class _AddReviewPageState extends State<AddReviewPage> {
  String reviewTitle = "";
  String reviewDescription = "";
  int rating = 0;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.fromLTRB(30, 60, 30, 0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                        addReview(context);
                      },
                      child: Container(
                        height: 35,
                        width: MediaQuery.of(context).size.width / 3.5,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          gradient: LinearGradient(
                            colors: [
                              Color(0xFF6380E4),
                              Color(0xFFA547DA),
                            ],
                          ),
                        ),
                        child: Center(
                            child: Text(
                          "Submit",
                          style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white),
                        )),
                      ),
                    ),
                  ],
                ),
                editTitleTextField(),
                SizedBox(
                  height: 30,
                ),
                editDescriptionTextField(),
                SizedBox(
                  height: 30,
                ),
                Center(
                  child: Container(
                    width: 250,
                    height: 50,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemBuilder: (context, j) {
                        return IconButton(
                          padding: EdgeInsets.zero,
                          onPressed: () {
                            setState(() {
                              rating = j + 1;
                            });
                          },
                          icon: Icon(
                            j + 1 <= rating ? Icons.star : Icons.star_border,
                            size: 40,
                            color: j + 1 <= rating ? Colors.amberAccent : Colors.grey,
                          ),
                        );
                      },
                      itemCount: 5,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget editTitleTextField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Title",
          style: GoogleFonts.roboto(fontSize: 20),
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          textAlign: TextAlign.start,
          maxLines: 1,
          validator: (val) {
            if (val.isEmpty) {
              return "This field should not be empty";
            }
            return null;
          },
          onChanged: (newValue) {
            reviewTitle = newValue;
          },
          decoration: InputDecoration(
            alignLabelWithHint: true,
            labelText: "Review Title",
            labelStyle: GoogleFonts.roboto(
              fontSize: 18,
            ),
            border: OutlineInputBorder(),
            filled: true,
            fillColor: Color(0xfff3f3f3),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Theme.of(context).accentColor,
                width: 2.0,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xFF6380E4),
                width: 2.0,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget editDescriptionTextField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Description",
          style: GoogleFonts.roboto(fontSize: 20),
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          textAlign: TextAlign.start,
          maxLines: 5,
          validator: (val) {
            if (val.isEmpty) {
              return "This field should not be empty";
            }
            return null;
          },
          onChanged: (newValue) {
            reviewDescription = newValue;
          },
          decoration: InputDecoration(
            alignLabelWithHint: true,
            labelText: "Review Description",
            labelStyle: GoogleFonts.roboto(
              fontSize: 15,
            ),
            border: OutlineInputBorder(),
            filled: true,
            fillColor: Color(0xfff3f3f3),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Theme.of(context).accentColor,
                width: 2.0,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xFF6380E4),
                width: 2.0,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> addReview(context) async {
    bool isValid = _formKey.currentState.validate();
    if (isValid) {
      if (rating != 0) {
        await EasyLoading.show(status: "Posting...");
        final res = await Provider.of<ReviewsState>(context, listen: false).postReview(widget.courseId, reviewTitle, reviewDescription, rating);
        if (res['error'] == false) {
          widget.addReview(res["id"], reviewTitle, reviewDescription, rating);
          await EasyLoading.dismiss();
          Navigator.pop(context);
        } else {
          EasyLoading.dismiss();
          return showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text("Something went wrong"),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'okay',
                    style: TextStyle(
                      color: Theme.of(context).secondaryHeaderColor,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          );
        }
      } else {
        return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text("Must provide a valid rating"),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Okay',
                  style: TextStyle(
                    color: Color(0xFF6A80E4),
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        );
      }
    }
  }
}

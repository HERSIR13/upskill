import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/models/postModel.dart';
import 'package:upSkill/models/subjectModel.dart';
import 'package:upSkill/pages/addPostPage.dart';
import 'package:upSkill/pages/postPage.dart';
import 'package:upSkill/states/postsState.dart';
import 'package:upSkill/states/subjectsState.dart';
import 'package:upSkill/widgets/fullPhoto.dart';

class CommunityPage extends StatefulWidget {
  @override
  _CommunityPageState createState() => _CommunityPageState();
}

class _CommunityPageState extends State<CommunityPage> {
  List<Post> posts = [];
  List<Subject> subjects = [];
  int currentSubject;
  bool isLoading = true;

  @override
  void initState() {
    _readyPage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.grey[200],
          body: !isLoading
              ? Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 90.0, bottom: 70),
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemBuilder: (context, i) => currentSubject == posts[i].subjectId
                            ? GestureDetector(
                                onTap: () {
                                  setState(() {
                                    posts[i].views++;
                                  });
                                  Navigator.of(context).push(
                                    SlideRightRoute(
                                      page: PostPage(
                                        postId: posts[i].id,
                                      ),
                                    ),
                                  );
                                },
                                child: Container(
                                  margin: EdgeInsets.all(16),
                                  child: ListTile(
                                    contentPadding: EdgeInsets.all(20),
                                    tileColor: Colors.white,
                                    title: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  width: 180,
                                                  child: Text(
                                                    posts[i].title,
                                                    overflow: TextOverflow.ellipsis,
                                                    style: GoogleFonts.roboto(fontSize: 20),
                                                  ),
                                                ),
                                                Text(
                                                  posts[i].studentName,
                                                  style: GoogleFonts.roboto(fontSize: 15, color: Colors.grey[600]),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: [
                                                Text(
                                                  posts[i].subjectName,
                                                  style: GoogleFonts.roboto(fontSize: 12),
                                                ),
                                                Text(
                                                  "20/11/2021",
                                                  style: GoogleFonts.roboto(fontSize: 15, color: Colors.grey[600]),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    subtitle: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        if (posts[i].image != null)
                                          InkWell(
                                            onTap: () {
                                              Navigator.of(context).push(
                                                SlideRightRoute(
                                                  page: FullPhoto(
                                                    url: posts[i].image,
                                                  ),
                                                ),
                                              );
                                            },
                                            child: Center(child: Image.network(posts[i].image)),
                                          ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          posts[i].description,
                                          style: GoogleFonts.roboto(fontSize: 14),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Icon(
                                              Icons.arrow_circle_up_outlined,
                                              color: Colors.blue,
                                            ),
                                            Text(
                                              posts[i].votesUp.toString(),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Icon(
                                              Icons.arrow_circle_down_outlined,
                                              color: Colors.red,
                                            ),
                                            Text(
                                              posts[i].votesDown.toString(),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Icon(
                                              Icons.visibility,
                                              color: Colors.grey,
                                            ),
                                            Text(
                                              posts[i].views.toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            : Container(),
                        itemCount: posts.length,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 15),
                      width: MediaQuery.of(context).size.width,
                      height: 90,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, j) => InkWell(
                          onTap: () => _changeSubject(subjects[j].id),
                          child: AnimatedContainer(
                            margin: EdgeInsets.all(8),
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              gradient: currentSubject == subjects[j].id
                                  ? LinearGradient(
                                      colors: [
                                        Color(0xFF6A80E4),
                                        Color(0xFF26B7ED),
                                      ],
                                    )
                                  : null,
                            ),
                            duration: Duration(milliseconds: 250),
                            child: Center(
                              child: Text(
                                subjects[j].name,
                                style: GoogleFonts.roboto(
                                  fontWeight: FontWeight.bold,
                                  color: currentSubject == subjects[j].id ? Colors.white : Colors.blueAccent,
                                ),
                              ),
                            ),
                          ),
                        ),
                        shrinkWrap: true,
                        itemCount: subjects.length,
                      ),
                    ),
                  ],
                )
              : Container(),
        ),
        Positioned(
          bottom: 15,
          left: 10,
          right: 10,
          child: Container(
            height: 60,
            decoration: BoxDecoration(
              boxShadow: [BoxShadow(blurRadius: 8, spreadRadius: 1, color: Colors.grey[400])],
              color: Colors.white,
            ),
            padding: EdgeInsets.all(5),
            width: 200,
            child: Row(
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      Navigator.of(context).push(
                        SlideRightRoute(
                          page: AddPostPage(
                            subjectId: currentSubject,
                            refreshPost: _refreshPosts,
                            isComment: false,
                          ),
                        ),
                      );
                    },
                    child: Container(
                      height: 50,
                      padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color(0xFF6A80E4),
                            Color(0xFF26B7ED),
                          ],
                        ),
                      ),
                      child: Center(
                        child: Text(
                          "Add Post",
                          style: GoogleFonts.roboto(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void _readyPage() {
    _getSubjects();
    _getPosts();
  }

  Future<void> _getPosts() async {
    EasyLoading.show(status: "Loading...");
    final res = await Provider.of<PostsState>(context, listen: false).getPostsData(context);
    if (res['error'] == false) {
      posts = res['posts'].reversed.toList();
      setState(() {
        isLoading = false;
      });
      EasyLoading.dismiss();
    } else {
      setState(() {
        isLoading = false;
      });
      EasyLoading.dismiss();
    }
  }

  void _getSubjects() {
    subjects = Provider.of<SubjectsState>(context, listen: false).allSubjects;
    currentSubject = subjects[0].id;
  }

  _changeSubject(int chosenSubject) {
    setState(() {
      currentSubject = chosenSubject;
    });
  }

  _refreshPosts() {
    setState(() {
      isLoading = true;
    });
    _getPosts();
  }
}

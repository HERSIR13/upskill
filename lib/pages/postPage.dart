import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/models/commentModel.dart';
import 'package:upSkill/models/postModel.dart';
import 'package:upSkill/states/postsState.dart';
import 'package:upSkill/widgets/fullPhoto.dart';

import 'addPostPage.dart';

class PostPage extends StatefulWidget {
  final int postId;

  PostPage({this.postId});

  @override
  _PostPageState createState() => _PostPageState();
}

class _PostPageState extends State<PostPage> {
  Post post = new Post();
  List<Comment> comments = [];
  bool isLoading = true;

  @override
  void initState() {
    _getPost();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.grey[200],
          body: !isLoading
              ? Padding(
                  padding: const EdgeInsets.only(top: 14.0),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.all(16),
                        child: ListTile(
                          contentPadding: EdgeInsets.all(20),
                          tileColor: Colors.white,
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      post.title,
                                      style: GoogleFonts.roboto(fontSize: 20),
                                    ),
                                    Text(
                                      post.studentName,
                                      style: GoogleFonts.roboto(fontSize: 15, color: Colors.grey[600]),
                                    ),
                                    Text(
                                      post.subjectName,
                                      style: GoogleFonts.roboto(fontSize: 12),
                                    ),
                                    Text(
                                      "20/11/2021",
                                      style: GoogleFonts.roboto(fontSize: 15, color: Colors.grey[600]),
                                    ),
                                  ],
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [],
                              ),
                            ],
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              if (post.image != null)
                                InkWell(
                                  onTap: () {
                                    Navigator.of(context).push(
                                      SlideRightRoute(
                                        page: FullPhoto(
                                          url: post.image,
                                        ),
                                      ),
                                    );
                                  },
                                  child: Center(
                                    child: Container(width: 100, child: Image.network(post.image)),
                                  ),
                                ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                post.description,
                                style: GoogleFonts.roboto(fontSize: 14),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Row(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      _votePost("up");
                                    },
                                    child: Icon(
                                      Icons.arrow_circle_up_outlined,
                                      color: Colors.blue,
                                    ),
                                  ),
                                  Text(
                                    post.votesUp.toString(),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      _votePost("down");
                                    },
                                    child: Icon(
                                      Icons.arrow_circle_down_outlined,
                                      color: Colors.red,
                                    ),
                                  ),
                                  Text(
                                    post.votesDown.toString(),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Icon(
                                    Icons.visibility,
                                    color: Colors.grey,
                                  ),
                                  Text(
                                    post.views.toString(),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                "Comments",
                                style: GoogleFonts.roboto(fontSize: 18),
                              ),
                              Divider(),
                              Container(
                                height: post.image == null ? 330 : 200,
                                child: ListView.builder(
                                  physics: AlwaysScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemBuilder: (context, i) => Column(
                                    children: [
                                      ListTile(
                                        leading: CircleAvatar(
                                          foregroundImage: NetworkImage(
                                            comments[i].studentImage,
                                          ),
                                          maxRadius: 20,
                                        ),
                                        title: Text(comments[i].studentName),
                                        subtitle: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            if (comments[i].image != null)
                                              InkWell(
                                                onTap: () {
                                                  Navigator.of(context).push(
                                                    SlideRightRoute(
                                                      page: FullPhoto(
                                                        url: comments[i].image,
                                                      ),
                                                    ),
                                                  );
                                                },
                                                child: Center(child: Image.network(comments[i].image)),
                                              ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(comments[i].description),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Row(
                                              children: [
                                                GestureDetector(
                                                  onTap: () {
                                                    _voteComment("up", comments[i].id, i);
                                                  },
                                                  child: Icon(
                                                    Icons.arrow_circle_up_outlined,
                                                    color: Colors.blue,
                                                  ),
                                                ),
                                                Text(
                                                  comments[i].votesUp.toString(),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    _voteComment("down", comments[i].id, i);
                                                  },
                                                  child: Icon(
                                                    Icons.arrow_circle_down_outlined,
                                                    color: Colors.red,
                                                  ),
                                                ),
                                                Text(
                                                  comments[i].votesDown.toString(),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      )
                                    ],
                                  ),
                                  itemCount: comments.length,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(),
        ),
        Positioned(
          bottom: 15,
          left: 10,
          right: 10,
          child: Container(
            height: 60,
            decoration: BoxDecoration(
              boxShadow: [BoxShadow(blurRadius: 8, spreadRadius: 1, color: Colors.grey[400])],
              color: Colors.white,
            ),
            padding: EdgeInsets.all(5),
            width: 200,
            child: Row(
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      Navigator.of(context).push(
                        SlideRightRoute(
                          page: AddPostPage(
                            isComment: true,
                            postId: post.id,
                            refreshPost: _refreshPost,
                          ),
                        ),
                      );
                    },
                    child: Container(
                      height: 50,
                      padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color(0xFF6A80E4),
                            Color(0xFF26B7ED),
                          ],
                        ),
                      ),
                      child: Center(
                        child: Text(
                          "Comment",
                          style: GoogleFonts.roboto(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _getPost() async {
    EasyLoading.show(status: "Loading...");
    final res = await Provider.of<PostsState>(context, listen: false).getPostData(context, widget.postId);
    if (res['error'] == false) {
      post = res['post'];
      comments = post.comments.reversed.toList();
      setState(() {
        isLoading = false;
      });
      EasyLoading.dismiss();
    } else {
      setState(() {
        isLoading = false;
      });
      EasyLoading.dismiss();
    }
  }

  _refreshPost() {
    setState(() {
      isLoading = true;
    });
    _getPost();
  }

  _votePost(String vote) async {
    setState(() {
      if (vote == "up")
        post.votesUp++;
      else
        post.votesDown++;
    });
    final res = await Provider.of<PostsState>(context, listen: false).votePostOrComment(context, post.id, false, vote);
    if (res['error'] == false) {
      return;
    } else {
      setState(() {
        if (vote == "up")
          post.votesUp--;
        else
          post.votesDown--;
      });
    }
  }

  _voteComment(String vote, int commentId, int index) async {
    setState(() {
      if (vote == "up")
        comments[index].votesUp++;
      else
        comments[index].votesDown++;
    });
    final res = await Provider.of<PostsState>(context, listen: false).votePostOrComment(context, commentId, true, vote);
    if (res['error'] == false) {
      return;
    } else {
      setState(() {
        if (vote == "up")
          comments[index].votesUp--;
        else
          comments[index].votesDown--;
      });
    }
  }
}

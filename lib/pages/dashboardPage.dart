import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/pages/coursePage.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  List<List<String>> activitiesList = [];

  @override
  void initState() {
    _getLatestActivities();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 25, right: 25, top: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Latest Activity",
                style: GoogleFonts.roboto(
                  fontSize: 20,
                  fontWeight: FontWeight.w800,
                  color: Colors.grey[700],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, i) => Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(
                          SlideRightRoute(
                            page: CoursePage(
                              courseId: int.parse(activitiesList[i][0]),
                              fromLatestActivity: true,
                              lectureId: int.parse(activitiesList[i][1]),
                            ),
                          ),
                        );
                      },
                      child: Stack(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                  activitiesList[i][2],
                                ),
                                fit: BoxFit.cover,
                              ),
                            ),
                            width: MediaQuery.of(context).size.width,
                            height: 200,
                          ),
                          Positioned(
                            bottom: 0,
                            left: 0,
                            child: Container(
                              height: 60,
                              width: MediaQuery.of(context).size.width,
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      activitiesList[i][3],
                                      style: GoogleFonts.roboto(fontWeight: FontWeight.w800),
                                    ),
                                    SizedBox(
                                      height: 3,
                                    ),
                                    Text(
                                      "Lecture: ${activitiesList[i][4]}",
                                      style: GoogleFonts.roboto(fontWeight: FontWeight.w400, fontSize: 13),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
                itemCount: activitiesList.length,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _getLatestActivities() async {
    activitiesList = await SharedPreferencesHelper.getLatestActivity();
    activitiesList.removeWhere((element) => element.isEmpty);
    setState(() {});
  }
}

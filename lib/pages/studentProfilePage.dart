import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/models/profileModel.dart';
import 'package:upSkill/pages/interestsPage.dart';
import 'package:upSkill/states/profileState.dart';

class StudentProfilePage extends StatefulWidget {
  @override
  _StudentProfilePageState createState() => _StudentProfilePageState();
}

class _StudentProfilePageState extends State<StudentProfilePage> {
  Profile profile;
  bool isEdit = false;
  String newName = "";
  String newBiography = "";
  String newInterests = "";

  @override
  void initState() {
    _setProfileInfo();
    super.initState();
  }

  final Shader linearGradient = LinearGradient(
    colors: <Color>[
      Color(0xFF8564DF),
      Color(0xFFA547DA),
    ],
  ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: !isEdit
          ? SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.all(30.0),
                child: new LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                    return Column(
                      children: [
                        Row(
                          children: [
                            CircleAvatar(
                              foregroundImage: NetworkImage(
                                profile.avatar,
                              ),
                              minRadius: 55,
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Flexible(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ShaderMask(
                                    shaderCallback: (bounds) => LinearGradient(colors: [
                                      Color(0xFFF400CF),
                                      Color(0xFFA547DA),
                                    ]).createShader(
                                      Rect.fromLTWH(0, 0, bounds.width, bounds.height),
                                    ),
                                    child: Text(
                                      "${profile.name}",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 27,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    "Student",
                                    style: GoogleFonts.roboto(
                                        fontSize: (width / 20).floor().toDouble(), color: Colors.black54, fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.person_outline_outlined,
                              size: 40,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 10),
                              width: constraints.maxWidth - 60,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Biography",
                                    style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: (width / 20).floor().toDouble()),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    profile.biography ?? "Not specified",
                                    style: GoogleFonts.roboto(fontSize: (width / 30).floor().toDouble(), color: Colors.grey[500]),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.local_movies_outlined,
                              size: 40,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 10),
                              width: constraints.maxWidth - 60,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Interests",
                                    style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: (width / 20).floor().toDouble()),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    profile.interests.toString() ?? "Not specified",
                                    style: GoogleFonts.roboto(fontSize: (width / 30).floor().toDouble(), color: Colors.grey[500]),
                                  )
                                ],
                              ),
                            )
                          ],
                        )
                      ],
                    );
                  },
                ),
              ),
            )
          : SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        InkWell(
                          onTap: () async {
                            await EasyLoading.show(status: "Updating...");
                            await Provider.of<ProfileState>(context, listen: false).updateProfile(
                              interests: newInterests,
                              name: newName.isNotEmpty ? newName : null,
                              biography: newBiography.isNotEmpty ? newBiography : null,
                            );
                            await EasyLoading.dismiss();
                            setState(() {
                              isEdit = false;
                            });
                          },
                          child: Container(
                            height: 35,
                            width: MediaQuery.of(context).size.width / 3.5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              gradient: LinearGradient(
                                colors: [
                                  Color(0xFF6380E4),
                                  Color(0xFFA547DA),
                                ],
                              ),
                            ),
                            child: Center(
                                child: Text(
                              "Submit",
                              style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white),
                            )),
                          ),
                        ),
                      ],
                    ),
                    editNameTextField(),
                    SizedBox(
                      height: 30,
                    ),
                    editBiographyTextField(),
                    SizedBox(
                      height: 30,
                    ),
                    editInterests(),
                  ],
                ),
              ),
            ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.edit,
          color: Colors.black54,
        ),
        backgroundColor: Colors.white,
        onPressed: () {
          setState(() {
            isEdit = !isEdit;
          });
        },
      ),
    );
  }

  Widget editNameTextField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Name",
          style: GoogleFonts.roboto(fontSize: 20),
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          textAlign: TextAlign.start,
          maxLines: 1,
          validator: (val) {
            if (val.isEmpty) {
              return "This field should not be empty";
            }
            return null;
          },
          onChanged: (newValue) {
            newName = newValue;
          },
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: profile.name,
            hintStyle: GoogleFonts.roboto(
              fontSize: 18,
            ),
            border: OutlineInputBorder(),
            filled: true,
            fillColor: Color(0xfff3f3f3),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Theme.of(context).accentColor,
                width: 2.0,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xFF6380E4),
                width: 2.0,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget editBiographyTextField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Biography",
          style: GoogleFonts.roboto(fontSize: 20),
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          textAlign: TextAlign.start,
          maxLines: 5,
          validator: (val) {
            if (val.isEmpty) {
              return "This field should not be empty";
            }
            return null;
          },
          onChanged: (newValue) {
            newBiography = newValue;
          },
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: profile.biography,
            hintStyle: GoogleFonts.roboto(
              fontSize: 15,
            ),
            border: OutlineInputBorder(),
            filled: true,
            fillColor: Color(0xfff3f3f3),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Theme.of(context).accentColor,
                width: 2.0,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xFF6380E4),
                width: 2.0,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget editInterests() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Interests",
          style: GoogleFonts.roboto(fontSize: 20),
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Expanded(
              child: Text(
                newInterests ?? "Not Specified",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: GoogleFonts.roboto(fontSize: 15, color: Colors.grey[500]),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).push(
                  SlideRightRoute(
                    page: InterestsPage(
                      isEditProfile: true,
                      setNewInterests: _setNewInterests,
                    ),
                  ),
                );
              },
              child: Text(
                "edit...",
                style: GoogleFonts.roboto(fontSize: 20),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  void _setNewInterests(String finalNewInterests) {
    setState(() {
      newInterests = finalNewInterests;
    });
  }

  void _setProfileInfo() {
    profile = Provider.of<ProfileState>(context, listen: false).profile;
    newInterests = profile.interests;
  }
}

import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;

class Links {
  static final uri = DotEnv.env["BASE_URI"];
  static final url = DotEnv.env["BASE_URL"];
  static final Uri loginUri = Uri.https(uri, '/api/v1/authentication/login');
  static final Uri guestLoginUri = Uri.https(uri, '/api/v1/authentication/guest');
  static final Uri registerUri = Uri.https(uri, '/api/v1/authentication/register');
  static final Uri logoutUri = Uri.https(uri, '/api/v1/authentication/logout');
  static final Uri updateStudentUri = Uri.https(uri, '/api/v1/students/updateStudent');
  static final Uri getSubjectsUri = Uri.https(uri, '/api/v1/subjects/getSubjects');
  static final Uri storePostUri = Uri.https(uri, '/api/v1/posts/store');
  static final Uri storeCommentUri = Uri.https(uri, '/api/v1/comments/store');

  static Uri getSubjectDataUri(int subjectId, int withCourses, int withLectures) {
    return Uri.https(uri, '/api/v1/subjects/getSubject', {"id": "$subjectId", "withCourses": "$withCourses", "withLectures": "$withLectures"});
  }

  static Uri getExploreDataUri(int withCourseReviews) {
    return Uri.https(uri, '/api/v1/explore/getExplore', {"withCourseReviews": "$withCourseReviews"});
  }

  static Uri getExploreSearchDataUri(String search, int withCourseReviews) {
    return Uri.https(uri, '/api/v1/explore/getExploreSearch', {"search": "$search", "withCourseReviews": "$withCourseReviews"});
  }

  static Uri getCourseDataUri(int courseId, int withCourseReviews, int withLectures) {
    return Uri.https(
        uri, '/api/v1/courses/getCourse', {"id": "$courseId", "withLectures": "$withLectures", "withCourseReviews": "$withCourseReviews"});
  }

  static Uri getProfileDataUri(int studentId, int withCoursesSubscribed, int withCourseReviews) {
    return Uri.https(uri, '/api/v1/students/getStudent',
        {"id": "$studentId", "withCoursesSubscribed": "$withCoursesSubscribed", "withCourseReviews": "$withCourseReviews"});
  }

  static Uri postReviewUri(int id, String title, String description, int rating) {
    return Uri.https(uri, '/api/v1/courseReviews/store', {"id": "$id", "title": title, "description": "$description", "rating": "$rating"});
  }

  static Uri subscribeCourseUri(int courseId) {
    return Uri.https(uri, '/api/v1/students/subscribeCourse', {"course_id": "$courseId"});
  }

  static Uri getPostsUri(int withSubject, int withStudent, int withComments) {
    return Uri.https(uri, '/api/v1/posts/getPosts', {"withSubject": "$withSubject", "withStudent": "$withStudent", "withComments": "$withComments"});
  }

  static Uri getPostDataUri(int postId, int withSubject, int withStudent, int withComments) {
    return Uri.https(uri, '/api/v1/posts/getPost',
        {"id": "$postId", "withSubject": "$withSubject", "withStudent": "$withStudent", "withComments": "$withComments"});
  }

  static Uri votePostUri(int postId, int withSubject, int withStudent, int withComments, String vote) {
    return Uri.https(uri, '/api/v1/posts/vote',
        {"id": "$postId", "withSubject": "$withSubject", "withStudent": "$withStudent", "withComments": "$withComments", "vote": "$vote"});
  }

  static Uri voteCommentUri(int commentId, int withStudent, String vote) {
    return Uri.https(uri, '/api/v1/comments/vote', {"id": "$commentId", "withStudent": "$withStudent", "vote": "$vote"});
  }
}

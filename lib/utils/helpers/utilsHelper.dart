import 'dart:io';

class UtilsHelper {
  static Future<bool> hasInternet() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.toString().isNotEmpty && result[0].rawAddress.toString().isNotEmpty) {
        return true;
      }
    } catch (_) {
      return false;
    }
    return false;
  }
}

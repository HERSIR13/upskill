import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  static Future<void> saveDeviceToken({String token}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('deviceToken', token);
  }

  static Future<String> getDeviceToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('deviceToken') ?? '';
    return token;
  }

  static Future<void> saveAuthToken({String token}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('authToken', token);
  }

  static Future<void> setGuestStatus({bool isGuest}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('isGuest', isGuest);
  }

  static Future<void> setLatestActivity({int courseId, int lectureId, String courseImage, String courseName, String lectureName}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> newAct = [courseId.toString(), lectureId.toString(), courseImage, courseName, lectureName];
    List<String> act1 = prefs.getStringList('act1') ?? [];
    List<String> act2 = prefs.getStringList('act2') ?? [];
    List<String> act3 = prefs.getStringList('act3') ?? [];
    if (act1.isEmpty) {
      await prefs.setStringList('act1', newAct);
    } else if (act2.isEmpty) {
      if ((courseId.toString() != act1[0])) {
        await prefs.setStringList('act2', newAct);
      } else {
        await prefs.setStringList('act1', newAct);
      }
    } else if (act3.isEmpty) {
      if ((courseId.toString() != act2[0] && courseId.toString() != act1[0])) {
        await prefs.setStringList('act3', newAct);
      } else if (courseId.toString() == act1[0]) {
        await prefs.setStringList('act1', newAct);
      } else {
        await prefs.setStringList('act2', newAct);
      }
    } else {
      if ((courseId.toString() != act2[0] && courseId.toString() != act3[0] && courseId.toString() != act1[0])) {
        act3 = act2;
        act2 = act1;
        await prefs.setStringList('act1', newAct);
        await prefs.setStringList('act2', act2);
        await prefs.setStringList('act3', act3);
      } else if (courseId.toString() == act1[0]) {
        await prefs.setStringList('act1', newAct);
      } else if (courseId.toString() == act2[0]) {
        await prefs.setStringList('act2', newAct);
      } else if (courseId.toString() == act3[0]) {
        await prefs.setStringList('act3', newAct);
      }
    }
  }

  static Future<void> resetLatestActivity() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList('act1', []);
    await prefs.setStringList('act2', []);
    await prefs.setStringList('act3', []);
  }

  static Future<void> saveEmailPassword({String email, String password}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('email', email);
    await prefs.setString('password', password);
  }

  static Future getAuthToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String _authToken = prefs.getString('authToken') ?? '';
    return _authToken;
  }

  static Future getGuestStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _isGuest = prefs.getBool('isGuest') ?? '';
    return _isGuest;
  }

  static Future<List<List<String>>> getLatestActivity() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    List<String> act1 = prefs.getStringList('act1') ?? [];
    List<String> act2 = prefs.getStringList('act2') ?? [];
    List<String> act3 = prefs.getStringList('act3') ?? [];

    return [act1, act2, act3];
  }

  static Future getEmailPassword() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String email = prefs.getString('email') ?? '';
    String password = prefs.getString('password') ?? '';
    return {"email": email, "password": password};
  }

  static Future<void> resetAuthToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool res = await prefs.remove('authToken');
    return res;
  }

  static Future<void> addAlarmType(String alarmType) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List alarmTypes = [];
    String alarmTypesPref = prefs.getString('alarmType');
    if (alarmTypesPref != null) {
      alarmTypes = jsonDecode(alarmTypesPref);
    }
    alarmTypes.add(alarmType);
    bool res = await prefs.setString('alarmType', jsonEncode(alarmTypes));
    return res;
  }

  static Future<List> getAlarmTypes() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List res = [];
    var alarmTypesPref = prefs.getString('alarmType');
    if (alarmTypesPref != null) {
      res = jsonDecode(alarmTypesPref);
    }
    return res;
  }

  static Future<void> removeAlarmTypes(List<String> alarmTypes) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List alarmTypesPref = await jsonDecode(prefs.getString('alarmType'));
    alarmTypesPref.removeWhere((element) => alarmTypes.contains(element));
    bool res = await prefs.setString('alarmType', jsonEncode(alarmTypesPref));
    return res;
  }

  static Future<void> updateChattingWith(String peerId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (peerId == null) peerId = "none";
    prefs.setString('chattingWith', peerId);
  }

  static Future<void> updateMyPosition(double lat, double lng) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var myPosition = [lat, lng];
    prefs.setString("myPosition", myPosition.toString());
  }

  static Future getMyPosition() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var defaultPosition = [35.38264453, 37.57199889];
    var myPosition = prefs.getString("myPosition") != null ? json.decode(prefs.getString("myPosition")) : defaultPosition;
    return myPosition;
  }
}

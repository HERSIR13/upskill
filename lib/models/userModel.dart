import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:upSkill/utils/constants.dart';

class User {
  String name;
  String email;
  String password;

  User({this.name, this.email, this.password});

  User.login({this.email, this.password});

  User.guestLogin();

  User.register({this.name, this.email, this.password});

  User.info({this.name, this.email});

  Future<http.Response> loginUser() async {
    Uri uri = Links.loginUri;
    final http.Response response = await http.post(
      uri,
      headers: <String, String>{'Content-Type': 'application/json; charset=UTF-8', 'Accept': 'application/json'},
      body: jsonEncode(<String, String>{
        'email': this.email,
        'password': this.password,
      }),
    );
    return response;
  }

  Future<http.Response> guestLoginUser() async {
    Uri uri = Links.guestLoginUri;
    final http.Response response = await http.get(
      uri,
      headers: <String, String>{'Content-Type': 'application/json; charset=UTF-8', 'Accept': 'application/json'},
    );
    return response;
  }

  Future<http.Response> registerUser({String authToken}) async {
    Uri uri = Links.registerUri;
    final http.Response response = await http.post(
      uri,
      headers: <String, String>{'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $authToken'},
      body: jsonEncode(<String, String>{
        'name': this.name,
        'email': this.email,
        'password': this.password,
      }),
    );
    return response;
  }

  static Future<http.Response> logoutUser({String authToken}) async {
    Uri uri = Links.logoutUri;
    final response = await http.get(uri, headers: {'Accept': 'application/json', 'Authorization': 'Bearer $authToken'});
    return response;
  }

}

import 'package:upSkill/models/courseModel.dart';

class Profile {
  int id;
  int roleId;
  String name;
  String email;
  String avatar;
  String biography;
  String interests;
  List<Course> subscribedCourses;

  Profile({
    this.id,
    this.roleId,
    this.name,
    this.email,
    this.avatar,
    this.biography,
    this.interests,
    this.subscribedCourses,
  });
}

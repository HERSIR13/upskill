import 'package:upSkill/models/commentModel.dart';

class Post {
  int id;
  int subjectId;
  String subjectName;
  int studentId;
  String studentName;
  String title;
  String description;
  String image;
  int views;
  int votesUp;
  int votesDown;
  int bestCommentId;
  List<Comment> comments;

  Post({
    this.id,
    this.subjectId,
    this.subjectName,
    this.studentId,
    this.studentName,
    this.title,
    this.description,
    this.image,
    this.views,
    this.votesUp,
    this.votesDown,
    this.bestCommentId,
    this.comments,
  });
}

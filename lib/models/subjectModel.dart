import 'package:upSkill/models/courseModel.dart';

class Subject {
  int id;
  String name;
  String image;
  String introductoryVideo;
  List<Course> courses = [];

  Subject({this.id, this.name, this.image, this.introductoryVideo, this.courses});
}

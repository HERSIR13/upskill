class Comment {
  int id;
  int postId;
  int studentId;
  String studentName;
  String studentImage;
  String title;
  String description;
  String image;
  int votesUp;
  int votesDown;
  int bestCommentId;

  Comment({
    this.id,
    this.postId,
    this.studentId,
    this.studentName,
    this.studentImage,
    this.title,
    this.description,
    this.image,
    this.votesUp,
    this.votesDown,
    this.bestCommentId,
  });
}

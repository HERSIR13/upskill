class Lecture {
  int id;
  int instructorId;
  int courseId;
  String name;
  String video;
  String description;
  String duration;

  Lecture({
    this.id,
    this.instructorId,
    this.courseId,
    this.name,
    this.video,
    this.description,
    this.duration,
  });
}

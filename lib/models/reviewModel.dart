class Review {
  int id;
  int courseId;
  int studentId;
  String studentName;
  String title;
  String description;
  int rating;

  Review({
    this.id,
    this.courseId,
    this.studentId,
    this.studentName,
    this.title,
    this.description,
    this.rating,
  });
}

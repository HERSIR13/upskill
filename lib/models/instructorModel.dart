class Instructor {
  int id;
  String name;
  String email;
  String avatar;
  String biography;
  String introductoryVideo;
  String interests;
  String education;
  String specialization;
  String achievements;
  String job;

  Instructor({
    this.id,
    this.name,
    this.email,
    this.avatar,
    this.biography,
    this.introductoryVideo,
    this.interests,
    this.education,
    this.specialization,
    this.achievements,
    this.job,
  });
}

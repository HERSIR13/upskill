import 'package:upSkill/models/lectureModel.dart';
import 'package:upSkill/models/reviewModel.dart';

import 'instructorModel.dart';

class Course {
  int id;
  int instructorId;
  int subjectId;
  String name;
  String image;
  String introductoryVideo;
  String description;
  int rating;
  String price;
  List<dynamic> objectives;
  String length;
  String level;
  int nbStudentsEnrolled;
  bool isBestSeller;
  bool isNew;
  int positivity;
  Instructor instructor;
  List<Lecture> lectures;
  List<Review> reviews;

  Course({
    this.id,
    this.instructorId,
    this.subjectId,
    this.name,
    this.image,
    this.introductoryVideo,
    this.description,
    this.rating,
    this.price,
    this.objectives,
    this.length,
    this.level,
    this.nbStudentsEnrolled,
    this.isBestSeller,
    this.isNew,
    this.positivity,
    this.instructor,
    this.lectures,
    this.reviews,
  });
}

import 'package:flutter/material.dart';

class ContactUsData {
  static List<Map> data = [
    {
      "title": "Number",
      "icon": Icons.smartphone_outlined,
      "details": "81 939 908",
    },
    {
      "title": "Email",
      "icon": Icons.email_outlined,
      "details": "help@upskill.com",
    },
    {
      "title": "Location",
      "icon": Icons.map_outlined,
      "details": "Lebanon, Mechref",
    },
  ];
}

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/pages/aboutUsPage.dart';
import 'package:upSkill/pages/contactUsPage.dart';
import 'package:upSkill/pages/splashPage.dart';
import 'package:upSkill/states/authenticationState.dart';

class CustomEndDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(20),
        topLeft: Radius.circular(20),
      ),
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
        width: 130,
        child: Container(
          padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
          height: 260,
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    SlideRightRoute(
                      page: ContactUsPage(),
                    ),
                  );
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.phone,
                      color: Colors.lightBlue,
                      semanticLabel: "Contact Us",
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Contact Us",
                      style: GoogleFonts.roboto(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Divider(
                thickness: 2,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(
                    SlideRightRoute(
                      page: AboutUsPage(),
                    ),
                  );
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.text_snippet,
                      color: Colors.lightBlue,
                      semanticLabel: "About Us",
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "About Us",
                      style: GoogleFonts.roboto(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              Divider(
                thickness: 2,
              ),
              InkWell(
                onTap: () async {
                  EasyLoading.show(status: "Logging out...");
                  final res = await Provider.of<AuthenticationState>(context, listen: false).logout(context);
                  if (res["error"] == false) {
                    EasyLoading.dismiss();
                    Navigator.of(context).pushReplacement(
                      SlideRightRoute(
                        page: SplashPage(),
                      ),
                    );
                  } else {
                    EasyLoading.dismiss();
                    return showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        title: Text("Something went wrong"),
                        actions: [
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              'okay',
                              style: TextStyle(
                                color: Theme.of(context).secondaryHeaderColor,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.exit_to_app,
                      color: Colors.lightBlue,
                      semanticLabel: "Logout",
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Logout",
                      style: GoogleFonts.roboto(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

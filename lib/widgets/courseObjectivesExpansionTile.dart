import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';

class CourseObjectivesExpansionTile extends StatelessWidget {
  final courseObjectives;

  CourseObjectivesExpansionTile({@required this.courseObjectives});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return ConfigurableExpansionTile(
      headerBackgroundColorStart: Colors.white,
      expandedBackgroundColor: Colors.white,
      initiallyExpanded: false,
      header: Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        width: width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.keyboard_arrow_down_outlined,
              color: Colors.grey[500],
              size: 35,
            ),
            Text(
              "Course Objectives",
              style: GoogleFonts.roboto(
                color: Colors.black87,
                fontSize: (width / 20).floor().toDouble(),
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      headerAnimationTween: Tween<double>(begin: 1, end: 1),
      headerExpanded: Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        width: width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.keyboard_arrow_up_outlined,
              color: Colors.grey[500],
              size: 35,
            ),
            Text(
              "Course Objectives",
              style: GoogleFonts.roboto(
                color: Colors.black87,
                fontSize: (width / 20).floor().toDouble(),
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      children: [
        Container(
          color: Colors.grey[200],
          width: width,
          height: 5,
        ),
        Container(
          width: width,
          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 25),
          child: LayoutBuilder(
            builder: (context, constraints) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, i) => Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              "${(i + 1).toString()}.",
                              style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 25),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 10),
                            width: constraints.maxWidth - 41,
                            child: Text(
                              courseObjectives[i].toString(),
                              style: GoogleFonts.roboto(fontSize: 13, fontWeight: FontWeight.w400),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                    ],
                  ),
                  itemCount: courseObjectives.length,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:upSkill/models/lectureModel.dart';

class CourseLecturesListViewBuilder extends StatefulWidget {
  final List<Lecture> lectures;
  final Function setIsLectureChosen;
  final int currentLectureIndex;

  CourseLecturesListViewBuilder({@required this.lectures,@required this.currentLectureIndex, @required this.setIsLectureChosen});

  @override
  _CourseLecturesListViewBuilderState createState() => _CourseLecturesListViewBuilderState();
}

class _CourseLecturesListViewBuilderState extends State<CourseLecturesListViewBuilder> {
  int lectureChosenIndex = 0;

  @override
  Widget build(BuildContext context) {
    if(widget.currentLectureIndex!=0)
      lectureChosenIndex=widget.currentLectureIndex;
    final width = MediaQuery.of(context).size.width;
    return Column(
      children: [
        Container(
          color: Colors.grey[200],
          width: width,
          padding: EdgeInsets.fromLTRB(25, 15, 25, 5),
          child: LayoutBuilder(
            builder: (context, constraints) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      lectureChosenIndex = 0;
                    });
                    widget.setIsLectureChosen(false);
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        color: Colors.white,
                        margin: EdgeInsets.symmetric(vertical: 5),
                        padding: EdgeInsets.fromLTRB(20, 0, 10, 10),
                        child: Container(
                          width: constraints.maxWidth - 30,
                          padding: EdgeInsets.only(top: 10),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                  "Introductory Video",
                                  style: GoogleFonts.roboto(fontSize: 16),
                                ),
                              ),
                              if (lectureChosenIndex == 0) Icon(Icons.play_arrow_sharp)
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, i) => InkWell(
                    onTap: () {
                      setState(() {
                        lectureChosenIndex = i + 1;
                      });
                      widget.setIsLectureChosen(true, lectureIndex: lectureChosenIndex);
                    },
                    child: Container(
                      color: Colors.white,
                      margin: EdgeInsets.symmetric(vertical: 5),
                      padding: EdgeInsets.fromLTRB(20, 0, 10, 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 10),
                            width: constraints.maxWidth - 30,
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    "Lecture ${i + 1}: ${widget.lectures[i].name}",
                                    style: GoogleFonts.roboto(fontSize: 16),
                                  ),
                                ),
                                if (lectureChosenIndex == i + 1) Icon(Icons.play_arrow_sharp)
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  itemCount: widget.lectures.length,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

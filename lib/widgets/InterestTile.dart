import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:progressive_image/progressive_image.dart';

class Interest extends StatefulWidget {
  final String interestName;
  final String interestImage;
  final LinearGradient interestGradient;
  final Function updateInterestsList;

  Interest({
    this.interestName,
    this.interestImage,
    this.interestGradient,
    this.updateInterestsList,
  });

  @override
  _InterestState createState() => _InterestState();
}

class _InterestState extends State<Interest> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  bool isChosen = false;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (!isChosen)
            widget.updateInterestsList(widget.interestName, true);
          else
            widget.updateInterestsList(widget.interestName, false);
          isChosen = !isChosen;
        });
      },
      child: AnimatedOpacity(
        opacity: isChosen ? 1 : 0.3,
        curve: Curves.fastOutSlowIn,
        duration: Duration(milliseconds: 500),
        child: Container(
          child: new LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return new Stack(
                children: [
                  Container(
                    child:ProgressiveImage(
                      placeholder: AssetImage('assets/images/Logo.png'),
                      // size: 1.87KB
                      thumbnail: AssetImage('assets/images/Logo.png'),
                      // size: 1.29MB
                      image: NetworkImage(widget.interestImage,),
                      width: constraints.maxWidth,
                      height: 200,
                    ),
                  ),
                  Positioned(
                    left: 0,
                    bottom: 0,
                    child: Container(
                      width: constraints.maxWidth,
                      color: Colors.white,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: Center(
                              child: Text(
                                widget.interestName,
                                style: GoogleFonts.roboto(fontSize: 15),
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              gradient: widget.interestGradient,
                            ),
                            height: 5,
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

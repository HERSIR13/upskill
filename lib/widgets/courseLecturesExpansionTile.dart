import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';
import 'package:upSkill/models/lectureModel.dart';

import 'courseLecturesListViewBuilder.dart';

class CourseLecturesExpansionTile extends StatelessWidget {
  final List<Lecture> lectures;
  final Function setIsLectureChosen;
  final int currentLectureIndex;

  CourseLecturesExpansionTile({@required this.lectures, @required this.currentLectureIndex, @required this.setIsLectureChosen});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return ConfigurableExpansionTile(
      headerBackgroundColorStart: Colors.white,
      expandedBackgroundColor: Colors.white,
      initiallyExpanded: false,
      header: Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        width: width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.keyboard_arrow_down_outlined,
              color: Colors.grey[500],
              size: 35,
            ),
            Text(
              "Lectures",
              style: GoogleFonts.roboto(
                color: Colors.black87,
                fontSize: (width / 20).floor().toDouble(),
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      headerAnimationTween: Tween<double>(begin: 1, end: 1),
      headerExpanded: Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        width: width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.keyboard_arrow_up_outlined,
              color: Colors.grey[500],
              size: 35,
            ),
            Text(
              "Lectures",
              style: GoogleFonts.roboto(
                color: Colors.black87,
                fontSize: (width / 20).floor().toDouble(),
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      children: [
        CourseLecturesListViewBuilder(
          lectures: lectures,
          setIsLectureChosen: setIsLectureChosen,
          currentLectureIndex: currentLectureIndex,
        ),
      ],
    );
  }
}

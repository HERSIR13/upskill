
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/pages/communityPage.dart';
import 'package:upSkill/pages/dashboardPage.dart';
import 'package:upSkill/pages/explorePage.dart';
import 'package:upSkill/pages/myCoursesPage.dart';
import 'package:upSkill/pages/signInPage.dart';
import 'package:upSkill/pages/studentProfilePage.dart';
import 'package:upSkill/states/profileState.dart';
import 'package:upSkill/utils/helpers/sharedPreferencesHelper.dart';

import 'endDrawer.dart';

class MainPageLayout extends StatefulWidget {
  @override
  _MainPageLayoutState createState() => _MainPageLayoutState();
}

class _MainPageLayoutState extends State<MainPageLayout> {
  int _currentIndex;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isGuest = true;

  @override
  void initState() {
    _getGuestStatus();
    _setProfile();
    _currentIndex = 1;
    super.initState();
  }

  Future<void> _getGuestStatus() async {
    isGuest = await SharedPreferencesHelper.getGuestStatus();
    if (isGuest == false) setState(() {});
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  List<Widget> allTabs = [
    DashboardPage(),
    ExplorePage(),
    CommunityPage(),
    MyCoursesPage(),
    StudentProfilePage(),
  ];

  List<String> allTabsTitles = [
    "Dashboard",
    "Explore",
    "Community",
    "My courses",
    "Profile",
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        key: _scaffoldKey,
        endDrawer: isGuest ? null : CustomEndDrawer(),
        // ignore: unrelated_type_equality_checks
        appBar: !isGuest
            ? AppBar(
                toolbarHeight: 60,
                backgroundColor: Colors.white,
                automaticallyImplyLeading: false,
                title: Text(
                  allTabsTitles[_currentIndex],
                  style: GoogleFonts.roboto(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 25),
                ),
                actions: [
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: IconButton(
                      onPressed: () => _scaffoldKey.currentState.openEndDrawer(),
                      icon: Icon(
                        Icons.menu,
                        color: Colors.grey[600],
                        size: 30,
                      ),
                    ),
                  ),
                ],
              )
            : null,
        body: allTabs[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          selectedFontSize: 10,
          unselectedIconTheme: IconThemeData(color: Colors.grey[400]),
          selectedItemColor: Colors.black,
          iconSize: 30,
          onTap: (index) {
            if (index != _currentIndex) {
              if (!isGuest)
                onTabTapped(index);
              else
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    backgroundColor: Colors.transparent,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                    contentPadding: EdgeInsets.zero,
                    content: Container(
                      height: 125,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        gradient: LinearGradient(
                          colors: [
                            Color(0xFF6380E4).withOpacity(0.9),
                            Color(0xFFA547DA).withOpacity(0.9),
                          ],
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pushReplacement(
                                SlideRightRoute(
                                  page: SignInPage(
                                    isSignUp: true,
                                  ),
                                ),
                              );
                            },
                            child: Text(
                              "Create an account?",
                              style: GoogleFonts.roboto(fontSize: 17, color: Colors.white, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Divider(
                            height: 10,
                            thickness: 2,
                            color: Colors.white,
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pushReplacement(
                                SlideRightRoute(
                                  page: SignInPage(
                                    isSignUp: false,
                                  ),
                                ),
                              );
                            },
                            child: Text(
                              "Already have an account?",
                              style: GoogleFonts.roboto(fontSize: 17, color: Colors.white, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
            }
          },
          type: BottomNavigationBarType.fixed,
          currentIndex: _currentIndex,
          // this will be set when a new tab is tapped
          items: [
            BottomNavigationBarItem(
                icon: Column(
                  children: [
                    new Icon(Icons.home_outlined),
                    SizedBox(
                      height: 1,
                    ),
                    Text(
                      "Dashboard",
                      style: GoogleFonts.roboto(fontSize: 12, fontWeight: FontWeight.bold, color: _currentIndex != 0 ? Colors.grey : Colors.black),
                    ),
                    SizedBox(
                      height: 1,
                    ),
                    if (_currentIndex == 0)
                      new Icon(
                        Icons.circle,
                        size: 8,
                        color: Color(0xFFEB08D0),
                      ),
                  ],
                ),
                label: 'Dashboard'),
            BottomNavigationBarItem(
                icon: Column(
                  children: [
                    new Icon(Icons.explore_outlined),
                    SizedBox(
                      height: 1,
                    ),
                    Text(
                      "Explore",
                      style: GoogleFonts.roboto(fontSize: 12, fontWeight: FontWeight.bold, color: _currentIndex != 1 ? Colors.grey : Colors.black),
                    ),
                    SizedBox(
                      height: 1,
                    ),
                    if (_currentIndex == 1)
                      new Icon(
                        Icons.circle,
                        size: 8,
                        color: Color(0xFFEB08D0),
                      ),
                  ],
                ),
                label: 'Explore'),
            BottomNavigationBarItem(
                icon: Column(
                  children: [
                    new Icon(Icons.comment_bank_outlined),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      "Community",
                      style: GoogleFonts.roboto(fontSize: 12, fontWeight: FontWeight.bold, color: _currentIndex != 2 ? Colors.grey : Colors.black),
                    ),
                    SizedBox(
                      height: 1,
                    ),
                    if (_currentIndex == 2)
                      new Icon(
                        Icons.circle,
                        size: 8,
                        color: Color(0xFFEB08D0),
                      ),
                  ],
                ),
                label: 'Community'),
            BottomNavigationBarItem(
                icon: Column(
                  children: [
                    new Icon(Icons.play_arrow_outlined),
                    SizedBox(
                      height: 1,
                    ),
                    Text(
                      "My courses",
                      style: GoogleFonts.roboto(fontSize: 12, fontWeight: FontWeight.bold, color: _currentIndex != 3 ? Colors.grey : Colors.black),
                    ),
                    SizedBox(
                      height: 1,
                    ),
                    if (_currentIndex == 3)
                      new Icon(
                        Icons.circle,
                        size: 8,
                        color: Color(0xFFEB08D0),
                      ),
                  ],
                ),
                label: 'My courses'),
            BottomNavigationBarItem(
              icon: Column(
                children: [
                  new Icon(Icons.person_outline_outlined),
                  SizedBox(
                    height: 1,
                  ),
                  Text(
                    "Profile",
                    style: GoogleFonts.roboto(fontSize: 12, fontWeight: FontWeight.bold, color: _currentIndex != 4 ? Colors.grey : Colors.black),
                  ),
                  SizedBox(
                    height: 1,
                  ),
                  if (_currentIndex == 4)
                    new Icon(
                      Icons.circle,
                      size: 8,
                      color: Color(0xFFEB08D0),
                    ),
                ],
              ),
              label: 'Profile',
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _setProfile() async {
    await Provider.of<ProfileState>(context, listen: false).getProfileData(context, isGuest);
  }
}

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/models/courseModel.dart';
import 'package:upSkill/pages/coursePage.dart';

class CourseTile extends StatelessWidget {
  final width;
  final isLast;
  final tileIndex;
  final Course course;

  CourseTile({this.width, this.isLast, this.tileIndex, this.course});

  @override
  Widget build(BuildContext context) {
    bool isBestSeller = false;
    bool isNew = false;
    String name = "Default course name";
    String instructorName = "Default Instructor";
    if (course != null) {
      isBestSeller = course.isBestSeller;
      isNew = course.isNew;
      name = course.name;
      instructorName = course.instructor.name;
    }
    return GestureDetector(
      onTap: () => Navigator.of(context).push(
        SlideRightRoute(
          page: CoursePage(
            courseId: course.id,
          ),
        ),
      ),
      child: Container(
        margin: EdgeInsets.only(left: 25, right: isLast ? 25 : 0),
        width: width / 1.8,
        color: Colors.blue,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                    course.image,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              height: 100,
            ),
            Expanded(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //just for demo, later we get it from backend
                    if (isBestSeller || isNew)
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          gradient: !isBestSeller
                              ? LinearGradient(
                                  colors: [
                                    Color(0xFF6A80E4),
                                    Color(0xFF26B7ED),
                                  ],
                                )
                              : LinearGradient(
                                  colors: [
                                    Color(0xFFF400CF),
                                    Color(0xFFA547DA),
                                  ],
                                ),
                        ),
                        child: Text(
                          isBestSeller
                              ? "Best Seller"
                              : isNew
                                  ? "New Course"
                                  : "",
                          style: GoogleFonts.roboto(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: (width / 30).floor().toDouble(),
                          ),
                        ),
                      ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      name,
                      style: GoogleFonts.roboto(
                        fontWeight: FontWeight.bold,
                        fontSize: (width / 25).floor().toDouble(),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      instructorName,
                      style: GoogleFonts.roboto(
                        fontWeight: FontWeight.bold,
                        fontSize: (width / 35).floor().toDouble(),
                        color: Colors.grey[600],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      course.description,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: GoogleFonts.roboto(
                        fontSize: (width / 37).floor().toDouble(),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                height: 20,
                                width: 105,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  itemBuilder: (context, j) {
                                    return Icon(
                                      j + 1 <= course.rating ? Icons.star : Icons.star_border,
                                      size: 20,
                                      color: j + 1 <= course.rating ? Colors.amberAccent : Colors.grey,
                                    );
                                  },
                                  itemCount: 5,
                                ),
                              ),
                              Text(
                                "(${course.rating.toDouble().toString()})",
                                style: GoogleFonts.roboto(
                                  fontSize: (width / 37).floor().toDouble(),
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[600],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 1,
                      height: 20,
                    ),
                    Center(
                      child: ShaderMask(
                        shaderCallback: (bounds) => LinearGradient(colors: [
                          Color(0xFF6A80E4),
                          Color(0xFF26B7ED),
                        ]).createShader(
                          Rect.fromLTWH(0, 0, bounds.width, bounds.height),
                        ),
                        child: Text(
                          "\$${course.price} USD",
                          style: TextStyle(
                            // The color must be set to white for this to work
                            color: Colors.white,
                            fontSize: (width / 20).floor().toDouble(),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:upSkill/animations/slideRightRoute.dart';
import 'package:upSkill/models/subjectModel.dart';
import 'package:upSkill/pages/subjectPage.dart';

class SubjectListView extends StatefulWidget {
  final List<Subject> subjects;

  SubjectListView({this.subjects});

  @override
  _SubjectListViewState createState() => _SubjectListViewState();
}

class _SubjectListViewState extends State<SubjectListView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, i) => GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
            Navigator.of(context).push(
              SlideRightRoute(
                page: SubjectPage(
                  subjectIndex: widget.subjects[i].id,
                ),
              ),
            );
          },
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 20, vertical: 7),
            color: Colors.white,
            height: 50,
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(
                        widget.subjects[i].image,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                  height: 50,
                  width: 50,
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  widget.subjects[i].name,
                ),
              ],
            ),
          ),
        ),
        itemCount: widget.subjects.length,
      ),
    );
  }
}

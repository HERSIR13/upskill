import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SigningField extends StatefulWidget {
  final String fieldName;
  final TextEditingController fieldController;
  final Function checkPasswords;
  final TextInputType keyboardType;

  SigningField({@required this.keyboardType, this.fieldName, this.fieldController, this.checkPasswords});

  @override
  _SigningFieldState createState() => _SigningFieldState();
}

class _SigningFieldState extends State<SigningField> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextFormField(
          controller: widget.fieldController,
          validator: (value) {
            if (widget.fieldName == "Email") {
              if (!value.contains("@"))
                return "Please enter a valid email";
              else
                return null;
            } else if (widget.fieldName == "Password") {
              if (value.isEmpty || value.length < 6)
                return "Please enter a password of at least 7 characters";
              else
                return null;
            } else if (widget.fieldName == "Username") {
              if (value.isEmpty)
                return "Please enter your Username";
              else
                return null;
            } else if (widget.fieldName == "Confirm Password") {
              if (value.isEmpty)
                return "Please Confirm Password";
              else if (!widget.checkPasswords())
                return "Confirmation should be same as the above password";
              else
                return null;
            } else if (widget.fieldName == "Name on Card") {
              if (value.isEmpty)
                return "Please enter the name found on your card";
              else
                return null;
            } else if (widget.fieldName == "Card Number") {
              if (value.isEmpty || value.length != 16)
                return "Please enter a card number of 16 characters";
              else
                return null;
            } else if (widget.fieldName == "Exp. Date") {
              if (value.isEmpty)
                return "Please enter an expiry date";
              else
                return null;
            } else if (widget.fieldName == "Security Code") {
              if (value.isEmpty || value.length != 3)
                return "Please enter a security code of 3 digits";
              else
                return null;
            } else
              return null;
          },
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 5, top: 10),
            isDense: true,
            hintText: widget.fieldName,
            hintStyle: GoogleFonts.roboto(fontSize: 15),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(style: BorderStyle.solid, color: Colors.black),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(style: BorderStyle.solid, color: Colors.purple),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(style: BorderStyle.solid, color: Colors.black),
            ),
          ),
          keyboardType: widget.keyboardType,
          obscureText: widget.fieldName == "Password" || widget.fieldName == "Confirm Password" ? true : false,
          onChanged: (_) {
            setState(() {});
          },
        ),
        SizedBox(
          height: 15,
        ),
      ],
    );
  }
}

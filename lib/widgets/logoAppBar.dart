import 'package:flutter/material.dart';

class LogoAppBar {
  static AppBar appBar = AppBar(
    toolbarHeight: 100,
    shadowColor: Colors.white54,
    elevation: 6,
    title: Image.asset(
      "assets/images/Logo.png",
      scale: 2.5,
    ),
    backgroundColor: Colors.white,
    centerTitle: true,
  );
}

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class Video extends StatefulWidget {
  final String videoUrl;
  final VideoPlayerController controller;
  final VideoProgressIndicator vPI;

  Video({this.videoUrl, this.controller, this.vPI});

  @override
  _VideoState createState() => _VideoState();
}

class _VideoState extends State<Video> {
  VideoProgressIndicator vPI;

  @override
  void initState() {
    print("______________init");
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    if (widget.controller != null) {
      widget.controller.dispose();
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      vPI = VideoProgressIndicator(
        widget.controller,
        allowScrubbing: true,
        colors: VideoProgressColors(backgroundColor: Colors.blue),
        padding: EdgeInsets.all(10),
      );
    });

    return AspectRatio(
      aspectRatio: widget.controller == null ? 1 : widget.controller.value.aspectRatio,
      child: LayoutBuilder(
        builder: (context, constraints) => Stack(
          alignment: Alignment.bottomCenter,
          children: [
            GestureDetector(
              onTap: () {
                setState(
                  () {
                    if (!widget.controller.value.isPlaying)
                      widget.controller.play();
                    else if (widget.controller.value.isPlaying) widget.controller.pause();
                  },
                );
              },
              child: Center(
                child: widget.controller.value.isInitialized
                    ? SizedBox.expand(
                        child: FittedBox(
                          fit: BoxFit.cover,
                          child: SizedBox(
                            width: constraints.maxWidth,
                            height: constraints.maxHeight,
                            child: VideoPlayer(widget.controller),
                          ),
                        ),
                      )
                    : Container(),
              ),
            ),
            if (!widget.controller.value.isPlaying)
              Center(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.6),
                    borderRadius: BorderRadius.circular(100),
                    gradient: LinearGradient(
                      colors: [
                        Color(0xFFA547DA),
                        Color(0xFFF400CF),
                      ],
                    ),
                  ),
                  child: IconButton(
                    icon: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 30,
                    ),
                    onPressed: () {
                      setState(
                        () {
                          if (!widget.controller.value.isPlaying)
                            widget.controller.play();
                          else if (widget.controller.value.isPlaying) widget.controller.pause();
                        },
                      );
                    },
                  ),
                ),
              ),
            if (widget.vPI == null) vPI,
            if (widget.vPI != null) widget.vPI,
          ],
        ),
      ),
    );
  }
}

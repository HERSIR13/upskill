import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';
import 'package:upSkill/models/reviewModel.dart';
import 'package:upSkill/widgets/reviewsListViewBuilder.dart';

class CourseReviewsExpansionTile extends StatelessWidget {
  final List<Review> reviews;

  CourseReviewsExpansionTile({@required this.reviews});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return ConfigurableExpansionTile(
      headerBackgroundColorStart: Colors.white,
      expandedBackgroundColor: Colors.white,
      initiallyExpanded: false,
      header: Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        width: width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.keyboard_arrow_down_outlined,
              color: Colors.grey[500],
              size: 35,
            ),
            Text(
              "Reviews",
              style: GoogleFonts.roboto(
                color: Colors.black87,
                fontSize: (width / 20).floor().toDouble(),
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      headerAnimationTween: Tween<double>(begin: 1, end: 1),
      headerExpanded: Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        width: width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.keyboard_arrow_up_outlined,
              color: Colors.grey[500],
              size: 35,
            ),
            Text(
              "Reviews",
              style: GoogleFonts.roboto(
                color: Colors.black87,
                fontSize: (width / 20).floor().toDouble(),
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      children: [
        Container(
          color: Colors.grey[200],
          width: width,
          height: 5,
        ),
        ReviewsListViewBuilder(
          reviews: reviews,
        ),
      ],
    );
  }
}

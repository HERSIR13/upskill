import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:upSkill/models/reviewModel.dart';

class ReviewsListViewBuilder extends StatefulWidget {
  final List<Review> reviews;

  ReviewsListViewBuilder({@required this.reviews});

  @override
  _ReviewsListViewBuilderState createState() => _ReviewsListViewBuilderState();
}

class _ReviewsListViewBuilderState extends State<ReviewsListViewBuilder> {
  int limit = 2;

  @override
  Widget build(BuildContext context) {
    final reviews = widget.reviews.reversed.toList();

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(20, 30, 20, 0),
          child: LayoutBuilder(builder: (context, constraints) {
            if (limit <= 0) limit = 2;
            return Column(
              children: [
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, i) => Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 15),
                        width: constraints.maxWidth,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: Center(
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 100,
                                    height: 50,
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      shrinkWrap: true,
                                      itemBuilder: (context, j) {
                                        return Icon(
                                          j + 1 <= reviews[i].rating ? Icons.star : Icons.star_border,
                                          color: j + 1 <= reviews[i].rating ? Colors.amberAccent : Colors.grey,
                                          size: 20,
                                        );
                                      },
                                      itemCount: 5,
                                    ),
                                  ),
                                ],
                              ),
                              Text(
                                reviews[i].title??"No title",
                                style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 17),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "by ${reviews[i].studentName}",
                                style: GoogleFonts.roboto(fontSize: 12),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 45.0),
                                child: Divider(
                                  height: 30,
                                  thickness: 0.35,
                                  color: Colors.black45,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0, 0, 20, 20),
                                child: Text(
                                  reviews[i].description??"No description",
                                  style: GoogleFonts.roboto(fontSize: 11, color: Colors.grey[700]),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                  itemCount: reviews.length > limit ? limit : reviews.length,
                ),
              ],
            );
          }),
        ),
        TextButton(
          onPressed: () {
            setState(() {
              if (limit < reviews.length)
                limit = limit + 5;
              else
                limit = limit - 10;
            });
          },
          child: Text(limit < reviews.length ? "Load more" : "Load less"),
        ),
      ],
    );
  }
}
